<?php
namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Event\Event;

class SubscribeController extends AppController{
	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
		$this->Auth->allow(['index']);
	}


	private function subscriberExits($email){
		$subscribers = TableRegistry::get("subscribers");
		$subscriber = $this->objectDecode($subscribers->find("All",['conditions'=>['email'=>$email]]));

		if(!empty($subscriber)){
			return true;
		}else{
			return false;
		}
	}

	public function index(){
		$subscribers = TableRegistry::get("subscribers");

		if($this->request->is('post')){
			$data = $this->request->data();

			if($this->subscriberExits($data['email'])){
				$this->Flash->success(__('Thank you for your subscription'));
			}else{
				$subscriber = $subscribers->newEntity();
				$subscriber->name = $data['name'];
				$subscriber->email = $data['email'];

				if($subscribers->save($subscriber)) {
					$this->Flash->success(__('Thank you for your subscription'));
				}else{
					$this->Flash->error(__('There was a problem saving your subscription details'));
				}
			}
			$this->redirect("/");
		}
	}
}
?>
