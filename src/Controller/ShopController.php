<?php
namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Phinx\Db\Table;

class ShopController extends AppController{
	public function beforeFilter(Event $event) {
		$this->Auth->allow(['index','product','index','products','categories','category']);
//		$this->setProductsPerPage(12); //Default Number Of Products Per Page
		$this->setUpUrl();
		return parent::beforeFilter($event);
	}

	public function index(){
		$products = $this->getProducts();
		$categories = $this->getCategories();

		$this->set('products',$products);

	}
	public function products(){
		$products = $this->getProducts();

		$products_preamble = [];
		foreach($products as $product){
			$products_preamble[$product['id']]['id'] = $product['id'];
			$products_preamble[$product['id']]['name'] = $product['name'];
			$products_preamble[$product['id']]['model'] = $product['model'];
			$products_preamble[$product['id']]['price'] = $product['price'];
			$products_preamble[$product['id']]['price_per'] = $product['pricing'];
			$products_preamble[$product['id']]['quantity'] = $product['quantity'];
			$products_preamble[$product['id']]['images'] = $this->getProductImages($product['id']);
		}

		//Set View Variables
		$this->set('products',$products_preamble);
	}
	public function product($id){

		//Set View Variables
		$this->set('productImages', $this->getProductImages($id));
		$this->set('product',$this->getProduct($id));
	}
	public function category(){}
	public function categories(){}
	public function cart(){}
	public function checkout(){}
	public function blog(){}
	public function article($id){}

	/**
	 * =========================================
	 * PRIVATE GET FUNCTIONS
	 * =========================================
	 */
	private function getProducts(){
		$products = TableRegistry::get('products');
		return $this->objectDecode($products->find('all'));
	}
	private function getManufacturer($id){
		$manufacturers = TableRegistry::get('manufacturers');
		return $this->objectDecode($manufacturers->get($id));
	}
	protected function getProduct($id){
		$products = TableRegistry::get('products');
		$product = $this->objectDecode($products->get($id));
		$product['manufacturer_id'] = $this->getManufacturer($product['manufacturer_id'])['name'];
		return $product;
	}
	private function getCategory($id){}
	private function getCategories(){
		$categories = TableRegistry::get('categories');
		return $this->objectDecode($categories->find('all'));
	}
	private function getProductImages($id) {
		$productImages = TableRegistry::get('product_images');
		$images =  $this->objectDecode($productImages->find('All',['conditions'=>['product_id'=>$id]]));
		return $images;
	}
	public function setUpUrl(){
		//Set Logged in User Session
		$this->set('user', $this->Auth->user());
		$this->Auth->allow(['display']);
		$url = explode('/', $this->request->here());
		if(!empty($url[2])) {
			if(strtolower($url[2]) == 'product') {
				$url[2] = 'Products';
				$url[3] = $this->getProduct($url[3])['name'];
			}
		}
		$this->set('url',$url);
	}
}
?>