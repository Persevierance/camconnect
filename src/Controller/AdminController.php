<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Phinx\Db\Table;

class AdminController extends AppController {
	public function index(){}

	/**
	 * -------------ADMIN CATEGORIES-------------------/
	 */
	public function categories(){
		$categories = TableRegistry::get("categories");

		$this->set('categories', $this->objectDecode($categories->find('All')));

	}

	public function addCategory(){
		$categories = TableRegistry::get("categories");
		$category = $categories->newEntity(); //New Instance of category

		if($this->request->is("post")){ //Check if the user has posted from the new category
			$data = $this->request->data();

			$category->name =               $data['name'];
			$category->sort_order =         $data['sort_order'];
			$category->meta_title =         $data['meta_title'];
			$category->meta_description =   $data['meta_description'];
			$category->date_added =         date("Y-m-d");
			$category->meta_keywords =      $data['meta_keywords'];
			$category->parent_id =          $data['parent'];
			$category->seo_description =    $data['seo_description'];

			if($categories->save($category)){
				$this->Flash->success(__("New category created successfully"));
				$this->redirect("/Admin/categories");
			}else{
				$this->Flash->error(__("There was a problem creating your category please try adain"));
			}
		}


		//Fix up another list for the Product Categories into a nice little readable array
		$categories = [];
		$categories['0']='Select One';
		foreach($this->getCategories() as $category){
			$categories[$category['id']] = $category['name'];
		}
		$this->set('categories',$categories);
	}

	public function category($id) {
		$categories = TableRegistry::get("categories");
		$category = $categories->get($id);
		$category = $this->objectDecode($category);

		$this->set('category',$category);
		$this->set('cat_orders',null);
		$this->set('top_products',null);
	}
	
	public function editCategory($id){
		$categories = TableRegistry::get("categories");
		$category = $categories->get($id);

		if($this->request->is('post')){
			$data = $this->request->data();
			$category = $categories->patchEntity($category, $data);
			if($categories->save($category)){
				$this->Flash->success(__('Category update successful'));
			}
		}
		$category = $this->objectDecode($category);
		$this->set('cat_orders',null);
		$this->set('top_products',null);
		$this->set('category',$category);
		$this->set('categories',$this->optionsCategoryList());
	}
	/**
	 * -------------ADMIN  PRODUCTS-------------------/
	 */
	public function products(){
		$products = $this->getProducts();
		$page = 0;
		$count = 0;
		$divider = 10;
		foreach($products as $product){
			if($count < $divider){
				$page = $page;}
			else{
				$page = $page + 1;
				$count = 0;
			}
			$count = $count + 1;
			$products_preamble[$page][$product['id']]['id'] = $product['id'];
			$products_preamble[$page][$product['id']]['name'] = $product['name'];
			$products_preamble[$page][$product['id']]['model'] = $product['model'];
			$products_preamble[$page][$product['id']]['price'] = $product['price'];
			$products_preamble[$page][$product['id']]['price_per'] = $product['pricing'];
			$products_preamble[$page][$product['id']]['quantity'] = $product['quantity'];
			$products_preamble[$page][$product['id']]['images'] = $this->getProductImages($product['id']);
			$products_preamble[$page][$product['id']]['status'] = $this->getProductStatus($product['status'])['name'];
		}

		$statuses = $this->getProductStatuses();
		$this->set('statuses',$statuses);
		$this->set('products',$products_preamble);
	}

	public function addProduct(){
		$products = TableRegistry::get("products");
		$product = $products->newEntity();
		if($this->request->is('post')) {
			$product = $products->patchEntity($product, $this->request->data());
			if($products->save($product)){
				$this->Flash->success(__('Product Saved'));
				$this->redirect('/Admin/Products');
			}else{
				$this->Flash->error(__('Error Saving Product'));
			}
		}

		//Fix up the Product Statuses into a nice little readable array
		$product_status_list = [];
		$product_status_list['empty'] = 'Select One';
		foreach($this->getProductStatuses() as $status) {
			$product_status_list[$status['id']] = $status['name'];
		}

		//Set Front End View Accessible Views
		$this->set('status_list',$product_status_list);
		$this->set('categories',$this->optionsCategoryList());

	}

	public function product($id){
		$productsTable = TableRegistry::get('products');
		$product = $productsTable->get($id);
		$this->set('product',$product);
	}

	public function editProduct($id){
		$products = TableRegistry::get('products');
		$productImages = TableRegistry::get('product_images');
		$product = $products->get($id);

		//PATCH POSTED PRODUCT
		if($this->request->is('post')) {
			$product = $products->patchEntity($product, $this->request->data());
			if($products->save($product)) {
				$this->Flash->success(__('Product information Saved'));
				$this->redirect('/Admin/Products');
			} else {
				$this->Flash->error(__('Error Saving Product'));
			}

			//UPLOAD AND PATCH IMAGE DETAILS
			if(isset($product['upload_image'])){
				$image = $this->request->data()['image'];

				$path = '../../webroot/img/products/';
				$title = strtoupper(str_replace(' ','_',$product['name'].'_'.substr(md5(rand()), 0, 3)));
				$file = $image;
				$ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
				$ext_array = ['jpg', 'jpeg', 'png', 'gif'];
				$new_image_name = $title.'.'.$ext;

				if(in_array($ext,$ext_array)){
					if(move_uploaded_file($file['tmp_name'],
						WWW_ROOT . $path . $new_image_name)){//File Moved Successfully
						$product_image = $productImages->newEntity();

						$image['product_id'] = $id;
						$image['name'] = $new_image_name;
						$product_image = $productImages->patchEntity($product_image,$image);

						if($productImages->save($product_image)){
							$this->Flash->success(__('Image Added Successfully'));
							$this->redirect('Admin/editProduct/'.$id);
						}else{$this->Flash->error(__('Sorry There was an error while trying to save your image please try again'));}
					}else {$this->Flash->error(__('Sorry There was an error while trying to save your image please try again'));}
				}else{$this->Flash->error(__('Unknown Image Type Please check your file and try again'));}
			}
		}

		//Fix up the Product Statuses into a nice little readable array
		$product_status_list = [];
		$product_status_list['empty'] = 'Select One';
		foreach($this->getProductStatuses() as $status) {
			$product_status_list[$status['id']] = $status['name'];
		}

		//Set Front End View Accessible Views
		$this->set('status_list',$product_status_list);

		$this->set('product',$product);
		$this->set('productImages', $this->getProductImages($id));
		$this->set('categories',$this->optionsCategoryList());
	}



	/**
	 * ====================================
	 * Private Get Methods
	 * ====================================
	 */

	private function getProducts(){
		$productsTable = TableRegistry::get('products');
		$products = $this->objectDecode($productsTable->find('All'));
		return $products;
	}

	private function getProductStatuses(){
		$statusTable = TableRegistry::get('product_status');
		return $this->objectDecode($statusTable->find('all'));
	}

	private function getProductStatus($id){
		$statusTable = TableRegistry::get('product_status');
		return $this->objectDecode($statusTable->get($id));
	}

	private function getCategories(){
		$categories = TableRegistry::get('categories');
		return $this->objectDecode($categories->find('all'));
	}

	private function getCategory($id){
		$categories = TableRegistry::get('categories');
		return $this->objectDecode($categories->get($id));
	}

	private function optionsCategoryList(){
		//Fix up another list for the Product Categories into a nice little readable array
		$categories = [];
		$categories['0'] = 'Select One';
		foreach($this->getCategories() as $category){
			$categories[$category['id']] = $category['name'];
		}

		return $categories;
	}

	/**
	 * ==================================
	 * Protected Get Methods
	 * ==================================
	 */

	protected function getProductImages($id) {
		$productImages = TableRegistry::get('product_images');
		return $this->objectDecode($productImages->find('All',['conditions'=>['product_id'=>$id]]));
	}
}
