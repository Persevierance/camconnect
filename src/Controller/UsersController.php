<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

class UsersController extends AppController{

	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
		$this->Auth->allow(['register']);
	}
	public function login() {
		if($this->Auth->user()){
			$this->Flash->success(__('Login Successful'));
			$this->redirect('/Shop/myAccount');
		}else{
			if($this->request->is('post')){
				$user = $this->Auth->identify();
				if($user != null){
					$this->Auth->setUser($user);
					$this->Flash->success(__('Login Successful'));
					if(strtolower($user['role']) == strtolower('admin')){
						return $this->redirect('/Admin  '); //Change to $this->Auth->redirectUrl() when you know how to
					}else {
						return $this->redirect('/Shop/myAccount'); //Change to $this->Auth->redirectUrl() when you know how to
					}
				}else{
					$this->Flash->error(__('Invalid username or password, Please try again'));
					return $this->redirect("/Users/login");
				}
			}
		}
	}
	public function register(){
		$user = $this->Users->newEntity();
		if ($this->request->is('post')) {
			$data = $this->request->data();
			if($data['password'] != $data['confirm_password']) {
				$this->Flash->error(__('Passwords do not match please try again'));
			} else {
				$this->request->data()['role'] = 'customer';
				$user = $this->Users->patchEntity($user, $this->request->data);
				if($this->Users->save($user)) {
					$this->Flash->success(__('Your account was successfully created.'));
					return $this->redirect(['action' => 'login']);
				}
				$this->Flash->error(__('Unable to add the user.'));
			}
		}
		$this->set('user', $user);
	}
	public function logout(){
		$this->Auth->logout();
		$this->Flash->success(__('Logout Successful'));
		$this->redirect('/Users/login');
	}
}