<?php
/**
 * Created by PhpStorm.
 * User: instantia
 * Date: 18/07/2016
 * Time: 21:00
 */
namespace App\Controller;
use App\Controller\Controller;

class UserController extends AppController{
	public function index(){}
	public function accountInformation(){}
	public function changePassword(){}
	public function addressBook(){}
	public function orderHistory(){}
}