<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class CategoriesTable extends Table {

	public function validationDefault(Validator $validator) {
		return $validator
			->notEmpty('name', 'The category name is required')
			->notEmpty('meta_title','Meta Tag Title is required')
			->notEmpty('description','The category is required')
			->notEmpty('parent','Please select a parent category');
	}

}
