<?php
//src/Model/Table/UsersTable.php
namespace App\Model\Table;


use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductsTable extends Table{
	public function validationDefault(Validator $validator) {
		return $validator
			->notEmpty('name', 'The Product Name Can Not be empty')
			->requirePresence('name');
	}
}
?>