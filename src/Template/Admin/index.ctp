<?php
	$this->layout = false;
	$active_nav = "dashboard";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>The Cam Connect Dashboard </title>

	<!-- Bootstrap -->
	<link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- iCheck -->
	<link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	<!-- bootstrap-progressbar -->
	<link href="/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
	<!-- jVectorMap -->
	<link href="/css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>

	<!-- Custom Theme Style -->
	<link href="/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
	<div class="main_container">
		<div class="col-md-3 left_col">
			<div class="left_col scroll-view">
				<div class="navbar nav_title" style="border: 0;">
					<a href="" class="site_title">
						<i class="fa fa-camera"></i>
						<span>TheCam<span class="text-warning small"><em>Connect</em></span></span>
					</a>
				</div>

				<div class="clearfix"></div>

				<!-- menu profile quick info -->
				<div class="profile">
					<div class="profile_pic">
						<img src="/img/img.jpg" alt="..." class="img-circle profile_img">
					</div>
					<div class="profile_info">
						<span>Welcome,</span>
						<h2><?=$user['firstname'] .' '. $user['lastname'];?></h2>
					</div>
				</div>
				<!-- /menu profile quick info -->

				<br />

				<!-- sidebar menu -->
				<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
					<div class="menu_section">
						<h3>Administrator</h3>
						<ul class="nav side-menu">
							<li><a class="active" href="/Admin/"><i class="fa fa-dashboard"></i> Dashboard </span></a>
							<li><a><i class="fa fa-tags"></i> Categories <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/Categories">View Categories</a></li>
									<li><a href="/Admin/AddCategory">Add New Category</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-camera-retro"></i> Products <span class="fa fa-chevron-down pull-right"></span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/Products/">Products</a></li>
									<li><a href="/Admin/AddProduct">Add New Product</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-desktop"></i> Design <span class="fa fa-chevron-down pull-right"></span><span class="label label-success pull-right">Coming Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/Layout">Layout</a></li>
									<li><a href="/Admin/Banner">Banner</a></li>
									<li><a href="/Admin/Media">Media</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-shopping-cart"></i> Sales <span class="fa fa-chevron-down pull-right"></span><span class="label label-success pull-right">Coming Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/SalesOrders">Orders</a></li>
									<li><a href="/Admin/RecurringOrders">Recurring Orders</a></li>
									<li><a href="/Admin/SalesReturns">Returns</a></li>
									<li><a href="/Admin/GiftVouchers">Gift Vouchers</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-user"></i> Customers <span class="fa fa-chevron-down pull-right"></span><span class="label label-success pull-right">C.Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/Customers">Customers</a></li>
									<li><a href="/Admin/CustomerGroups">Customer Groups</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-bar-chart"></i>Report <span class="fa fa-chevron-down pull-right"></span><span class="label label-success pull-right">Coming Soon</span> </a>
								<ul class="nav child_menu">
									<li><a href="/Admin/SalesReport">Sales</a></li>
									<li><a href="/Admin/CustomersReport/">Customers</a></li>
									<li><a href="/Admin/MarketingReport/">Marketing</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-gears"></i>System <span class="fa fa-chevron-down pull-right"></span><span class="label label-success pull-right">Coming Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/SystemSettings/">Settings</a></li>
									<li><a href="/Admin/SystemUsers/">Users</a></li>
									<li><a href="/Admin/SystemTools/">Tools</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /sidebar menu -->

				<!-- /menu footer buttons -->
				<div class="sidebar-footer hidden-small">
					<a data-toggle="tooltip" data-placement="top" title="Settings">
						<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
					</a>
					<a data-toggle="tooltip" data-placement="top" title="FullScreen">
						<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
					</a>
					<a data-toggle="tooltip" data-placement="top" title="Lock">
						<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
					</a>
					<a data-toggle="tooltip" data-placement="top" title="Logout">
						<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
					</a>
				</div>
				<!-- /menu footer buttons -->
			</div>
		</div>

		<!-- top navigation -->
		<div class="top_nav">
			<div class="nav_menu">
				<nav>
					<div class="nav toggle">
						<a id="menu_toggle"><i class="fa fa-bars"></i></a>
					</div>

					<ul class="nav navbar-nav navbar-right">
						<li class="">
							<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<img src="/img/img.jpg" alt=""><?=$user['firstname'] .' '. $user['lastname'];?>
								<span class=" fa fa-angle-down"></span>
							</a>
							<ul class="dropdown-menu dropdown-usermenu pull-right">
								<li><a href="javascript:;"> Profile</a></li>
								<li>
									<a href="javascript:;">
										<span class="badge bg-red pull-right">50%</span>
										<span>Settings</span>
									</a>
								</li>
								<li><a href="javascript:;">Help</a></li>
								<li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
							</ul>
						</li>

						<li role="presentation" class="dropdown">
							<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
								<i class="fa fa-envelope-o"></i>
								<span class="badge bg-green">6</span>
							</a>
							<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
								<li>
									<a>
										<span class="image"><img src="/img/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
									</a>
								</li>
								<li>
									<a>
										<span class="image"><img src="/img/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
									</a>
								</li>
								<li>
									<a>
										<span class="image"><img src="/img/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
									</a>
								</li>
								<li>
									<a>
										<span class="image"><img src="/img/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
									</a>
								</li>
								<li>
									<div class="text-center">
										<a>
											<strong>See All Alerts</strong>
											<i class="fa fa-angle-right"></i>
										</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<!-- top tiles -->
			<div class="row tile_count">
				<div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
					<span class="count_top"><i class="fa fa-user"></i> Total Customers</span>
					<div class="count">320</div>
					<span class="count_bottom"><i class="green"><a href="/Admin/System/Users"> View More </a></i></span>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
					<span class="count_top"><i class="fa fa-clock-o"></i> Products Hired</span>
					<div class="count">33</div>
					<span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>5% </i> Of All Total Products</span>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
					<span class="count_top"><i class="fa fa-shopping-cart"></i> Weekly Sales</span>
					<div class="count green">50</div>
					<span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> Of All Monthly</span>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
					<span class="count_top"><i class="fa fa-camera-retro"></i> Total Products</span>
					<div class="count">527</div>
					<span class="count_bottom"><i class="green"><i class="fa fa-plus"></i>95% </i> Available In Stock</span>
				</div>
			</div>
			<!-- /top tiles -->

			<div class="row">
				<div class="col-md-12 text-center"><?=$this->Flash->render()?></div>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<div class="x_panel tile fixed_height_320">
						<div class="x_title">
							<h2><i class="fa fa-shopping-cart"></i> Latest Orders</h2>
							<ul class="nav navbar-right panel_toolbox">
								<li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<div class="table-responsive">
								<table class="table table-striped jambo_table bulk_action">
									<thead>
									<tr class="headings">
										<th class="column-title" style="display: table-cell;">Invoice </th>
										<th class="column-title" style="display: table-cell;">Invoice Date </th>
										<th class="column-title" style="display: table-cell;">Order </th>
										<th class="column-title" style="display: table-cell;">Bill to Name </th>
										<th class="column-title" style="display: table-cell;">Status </th>
										<th class="column-title" style="display: table-cell;">Amount </th>
										<th class="column-title no-link last" style="display: table-cell;"><span class="nobr">Action</span>
										</th>
										<th class="bulk-actions" colspan="7" style="display: none;">
											<a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt">1 Records Selected</span> ) <i class="fa fa-chevron-down"></i></a>
										</th>
									</tr>
									</thead>
									<tbody>
										<tr class="even pointer">
											<td class=" ">121000040</td>
											<td class=" ">May 23, 2014 11:47:56 PM </td>
											<td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i></td>
											<td class=" ">John Blank L</td>
											<td class=" ">Paid</td>
											<td class="a-right a-right ">$7.45</td>
											<td class=" last"><a href="#">View</a>
											</td>
										</tr>
										<tr class="odd pointer">
											<td class=" ">121000039</td>
											<td class=" ">May 23, 2014 11:30:12 PM</td>
											<td class=" ">121000208 <i class="success fa fa-long-arrow-up"></i>
											</td>
											<td class=" ">John Blank L</td>
											<td class=" ">Paid</td>
											<td class="a-right a-right ">$741.20</td>
											<td class=" last"><a href="#">View</a>
											</td>
										</tr>
										<tr class="even pointer">
											<td class=" ">121000038</td>
											<td class=" ">May 24, 2014 10:55:33 PM</td>
											<td class=" ">121000203 <i class="success fa fa-long-arrow-up"></i>
											</td>
											<td class=" ">Mike Smith</td>
											<td class=" ">Paid</td>
											<td class="a-right a-right ">$432.26</td>
											<td class=" last"><a href="#">View</a>
											</td>
										</tr>
										<tr class="odd pointer">
											<td class=" ">121000037</td>
											<td class=" ">May 24, 2014 10:52:44 PM</td>
											<td class=" ">121000204</td>
											<td class=" ">Mike Smith</td>
											<td class=" ">Paid</td>
											<td class="a-right a-right ">$333.21</td>
											<td class=" last"><a href="#">View</a>
											</td>
										</tr>
										<tr class="even pointer">
											<td class=" ">121000040</td>
											<td class=" ">May 24, 2014 11:47:56 PM </td>
											<td class=" ">121000210</td>
											<td class=" ">John Blank L</td>
											<td class=" ">Paid</td>
											<td class="a-right a-right ">$7.45</td>
											<td class=" last"><a href="#">View</a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="x_panel tile fixed_height_320">
						<div class="x_title">
							<h2><i class="fa fa-filter"></i> Quick Options</h2>
							<ul class="nav navbar-right pull-right panel_toolbox">
								<li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
								<li class="dropdown"></li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<div class="dashboard-widget-content">
								<ul class="quick-list">
									<li><i class="fa fa-shopping-cart"></i><a href="/Admin/Sales">Sales</a></li>
									<li><i class="fa fa-gears"></i><a href="/Admin/Settings">Settings</a></li>
									<li><i class="fa fa-camera-retro"></i><a href="/Admin/Products">Products</a></li>
									<li><i class="fa fa-tags"></i><a href="/Admin/Categories">Categories</a></li>
									<li><i class="fa fa-bars"></i><a href="/Admin/Subscriptions">Subscription</a></li>
									<li><i class="fa fa-line-chart"></i><a href="/Admin/Achievements">Achievements</a></li>
									<li><i class="fa fa-sign-out"></i><a href="#">Logout</a></li>
								</ul>

								<div class="sidebar-widget">
									<h4>Profit Target</h4>
									<canvas width="150" height="80" id="foo" class="" style="width: 160px; height: 100px;"></canvas>
									<div class="goal-wrapper">
										<span class="gauge-value pull-left">£</span>
										<span id="gauge-text" class="gauge-value pull-left">3,200</span>
										<span id="goal-text" class="goal-value pull-right">£5,000</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2><i class="fa fa-calendar"></i> Recent Activities <small>Sessions</small></h2>
							<ul class="nav navbar-right panel_toolbox">
								<li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<div class="dashboard-widget-content">

								<ul class="list-unstyled timeline widget">
									<li>
										<div class="block">
											<div class="block_content">
												<h2 class="title">
													<a>You’ve Just Logged in</a>
												</h2>
												<div class="byline">
													<span>just now </span><a><?= $user['firstname'] .' '. $user['lastname']  ?></a>
												</div>
												<p class="excerpt">
													You and 0 other users are currently logged in.
												</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-8 col-sm-8 col-xs-12">
					<div class="row">
						<!-- Start to do list -->
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Returned Products <small>Booking In</small></h2>
									<ul class="nav navbar-right panel_toolbox pull-right">
										<li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<div class="">
									</div>
								</div>
							</div>
						</div>
						<!-- End to do list -->

						<!-- start of weather widget -->
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Awaiting Repairs</h2>
									<ul class="nav navbar-right panel_toolbox">
										<li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
								</div>
							</div>
						</div>
						<!-- end of weather widget -->
					</div>
				</div>
			</div>
		</div>
		<!-- /page content -->

		<!-- footer content -->
		<footer>
			<div class="text-center">
				The Cam Connect
			</div>
			<div class="clearfix"></div>
		</footer>
		<!-- /footer content -->
	</div>
</div>

<!-- jQuery -->
<script src="/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="/vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="/vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="/vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="/vendors/Flot/jquery.flot.js"></script>
<script src="/vendors/Flot/jquery.flot.pie.js"></script>
<script src="/vendors/Flot/jquery.flot.time.js"></script>
<script src="/vendors/Flot/jquery.flot.stack.js"></script>
<script src="/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="/js/flot/jquery.flot.orderBars.js"></script>
<script src="/js/flot/date.js"></script>
<script src="/js/flot/jquery.flot.spline.js"></script>
<script src="/js/flot/curvedLines.js"></script>
<!-- jVectorMap -->
<script src="/js/maps/jquery-jvectormap-2.0.3.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="/js/moment/moment.min.js"></script>
<script src="/js/datepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="/js/custom.min.js"></script>

<!-- jVectorMap -->
<script src="/js/maps/jquery-jvectormap-world-mill-en.js"></script>
<script src="/js/maps/jquery-jvectormap-us-aea-en.js"></script>
<script src="/js/maps/gdp-data.js"></script>
<script>
	$(document).ready(function(){
		$('#world-map-gdp').vectorMap({
			map: 'world_mill_en',
			backgroundColor: 'transparent',
			zoomOnScroll: false,
			series: {
				regions: [{
					values: gdpData,
					scale: ['#E6F2F0', '#149B7E'],
					normalizeFunction: 'polynomial'
				}]
			},
			onRegionTipShow: function(e, el, code) {
				el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
			}
		});
	});
</script>
<!-- /jVectorMap -->

<!-- Skycons -->
<script>
	$(document).ready(function() {
		var icons = new Skycons({
				"color": "#73879C"
			}),
			list = [
				"clear-day", "clear-night", "partly-cloudy-day",
				"partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
				"fog"
			],
			i;

		for (i = list.length; i--;)
			icons.set(list[i], list[i]);
		icons.play();
	});
</script>
<!-- /Skycons -->

<!-- gauge.js -->
<script>
	var opts = {
		lines: 12,
		angle: 0,
		lineWidth: 0.4,
		pointer: {
			length: 0.75,
			strokeWidth: 0.042,
			color: '#1D212A'
		},
		limitMax: 'false',
		colorStart: '#1ABC9C',
		colorStop: '#1ABC9C',
		strokeColor: '#F0F3F3',
		generateGradient: true
	};
	var target = document.getElementById('foo'),
		gauge = new Gauge(target).setOptions(opts);

	gauge.maxValue = 6000;
	gauge.animationSpeed = 32;
	gauge.set(3200);
	gauge.setTextField(document.getElementById("gauge-text"));
</script>
<!-- /gauge.js -->
</body>
</html>
