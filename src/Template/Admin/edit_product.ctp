<?php
$this->layout = false;
$active_nav = "dashboard";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>The Cam Connect Dashboard </title>

	<!-- Bootstrap -->
	<link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- iCheck -->
	<link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	<!-- bootstrap-progressbar -->
	<link href="/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
	<link href="/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
	<!-- jVectorMap -->
	<link href="/css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>

	<!-- Custom Theme Style -->
	<link href="/css/custom.min.css" rel="stylesheet">
	<link href="/css/admin/custom.css" rel="stylesheet">

	<!-- Dropzone.js -->
	<link href="/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">



</head>

<body class="nav-md">
<div class="container body">
	<div class="main_container">
		<div class="col-md-3 left_col">
			<div class="left_col scroll-view">
				<div class="navbar nav_title" style="border: 0;">
					<a href="" class="site_title">
						<i class="fa fa-camera"></i>
						<span>TheCam<span class="text-warning small"><em>Connect</em></span></span>
					</a>
				</div>

				<div class="clearfix"></div>

				<!-- menu profile quick info -->
				<div class="profile">
					<div class="profile_pic">
						<img src="/img/img.jpg" alt="..." class="img-circle profile_img">
					</div>
					<div class="profile_info">
						<span>Welcome,</span>
						<h2><?=$user['firstname'] .' '. $user['lastname'];?></h2>
					</div>
				</div>
				<!-- /menu profile quick info -->

				<br />

				<!-- sidebar menu -->
				<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
					<div class="menu_section">
						<h3>Administrator</h3>
						<ul class="nav side-menu">
							<li><a href="/Admin/"><i class="fa fa-dashboard"></i> Dashboard </span></a>
							<li class="active"><a><i class="fa fa-tags"></i> Categories <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li><a  href="/Admin/Categories">View Categories</a></li>
									<li><a href="/Admin/AddCategory">Add New Category</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-camera-retro"></i> Products <span class="fa fa-chevron-down pull-right"></span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/Products/">Products</a></li>
									<li><a href="/Admin/AddProduct">Add New Product</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-desktop"></i> Design <span class="fa fa-chevron-down pull-right"></span><span class="label label-success pull-right">Coming Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/Layout">Layout</a></li>
									<li><a href="/Admin/Banner">Banner</a></li>
									<li><a href="/Admin/Media">Media</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-shopping-cart"></i> Sales <span class="fa fa-chevron-down pull-right"></span><span class="label label-success pull-right">Coming Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/SalesOrders">Orders</a></li>
									<li><a href="/Admin/RecurringOrders">Recurring Orders</a></li>
									<li><a href="/Admin/SalesReturns">Returns</a></li>
									<li><a href="/Admin/GiftVouchers">Gift Vouchers</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-user"></i> Customers <span class="fa fa-chevron-down pull-right"></span><span class="label label-success pull-right">C.Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/Customers">Customers</a></li>
									<li><a href="/Admin/CustomerGroups">Customer Groups</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-bar-chart"></i>Report <span class="fa fa-chevron-down pull-right"></span><span class="label label-success pull-right">Coming Soon</span> </a>
								<ul class="nav child_menu">
									<li><a href="/Admin/SalesReport">Sales</a></li>
									<li><a href="/Admin/CustomersReport/">Customers</a></li>
									<li><a href="/Admin/MarketingReport/">Marketing</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-gears"></i>System <span class="fa fa-chevron-down pull-right"></span><span class="label label-success pull-right">Coming Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/SystemSettings/">Settings</a></li>
									<li><a href="/Admin/SystemUsers/">Users</a></li>
									<li><a href="/Admin/SystemTools/">Tools</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /sidebar menu -->

				<!-- /menu footer buttons -->
				<div class="sidebar-footer hidden-small">
					<a data-toggle="tooltip" data-placement="top" title="Settings">
						<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
					</a>
					<a data-toggle="tooltip" data-placement="top" title="FullScreen">
						<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
					</a>
					<a data-toggle="tooltip" data-placement="top" title="Lock">
						<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
					</a>
					<a data-toggle="tooltip" data-placement="top" title="Logout">
						<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
					</a>
				</div>
				<!-- /menu footer buttons -->
			</div>
		</div>

		<!-- top navigation -->
		<div class="top_nav">
			<div class="nav_menu">
				<nav>
					<div class="nav toggle">
						<a id="menu_toggle"><i class="fa fa-bars"></i></a>
					</div>
					<ul class="nav navbar-nav navbar-right">
						<li class="">
							<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<img src="/img/img.jpg" alt=""><?=$user['firstname'] .' '. $user['lastname'];?>
								<span class=" fa fa-angle-down"></span>
							</a>
							<ul class="dropdown-menu dropdown-usermenu pull-right">
								<li><a href="javascript:;"> Profile</a></li>
								<li>
									<a href="javascript:;">
										<span class="badge bg-red pull-right">50%</span>
										<span>Settings</span>
									</a>
								</li>
								<li><a href="javascript:;">Help</a></li>
								<li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
							</ul>
						</li>

						<li role="presentation" class="dropdown">
							<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
								<i class="fa fa-envelope-o"></i>
								<span class="badge bg-green">6</span>
							</a>
							<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
								<li>
									<a>
										<span class="image"><img src="/img/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
									</a>
								</li>
								<li>
									<a>
										<span class="image"><img src="/img/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
									</a>
								</li>
								<li>
									<a>
										<span class="image"><img src="/img/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
									</a>
								</li>
								<li>
									<a>
										<span class="image"><img src="/img/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
									</a>
								</li>
								<li>
									<div class="text-center">
										<a>
											<strong>See All Alerts</strong>
											<i class="fa fa-angle-right"></i>
										</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="row">
				<div class="x_panel"><h3><i class="fa fa-fw fa-camera-retro"></i><?=$product['name']?></h3></div>
				<div class="text-center"><?= $this->Flash->render();?></div>
				<div class="x_panel">
					<?=$this->Form->create();?>
						<div class="x_title">
							<h2>Edit <small><?=$product['name']?></small></h2>
							<ul class="nav navbar-right panel_toolbox">
								<li class=""><button class="btn btn-primary btn-xs"><i class="fa fa-check"></i> Save</button></li>
								<li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="" role="tabpanel" data-example-id="togglable-tabs">
							<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
								<li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">General </a></li>
								<li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Data </a></li>
								<li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Pricing </a></li>
							</ul>
							<div id="myTabContent" class="tab-content">
								<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
									<h2 class="text-center StepTitle">General Product Information</h2>
									<div class='form-horizontal form-label-left'>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product Name">Product Name<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'name',[
														'label'=>false,
														'type'=>'text',
														'required'=>'required',
														'value'=>$product['name'],
														'placeholder'=>'Product Name',
														'class'=>'form-control col-md-7 col-xs-12'
													]
												);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product Name">Product Description<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'description',[
														'rows'=>4,
														'label'=>false,
														'type'=>'textarea',
														'required'=>'required',
														'value'=>$product['description'],
														'placeholder'=>'Product Description',
														'class'=>'form-control col-md-7 col-xs-12'
													]
												);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product Name">Category<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
														'category_id',[
																'rows'=>4,
																'label'=>false,
																'type'=>'select',
																'required'=>'required',
																'options'=>$categories,
																'value'=>$product['category_id'],
																'class'=>'form-control col-md-7 col-xs-12'
														]
												);?>
											</div>

										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product Name">Meta Tag Title<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'meta_title',[
														'type'=>'text',
														'label'=>false,
														'required'=>'required',
														'placeholder'=>'Meta Tag Title',
														'value'=>$product['meta_title'],
														'class'=>'form-control col-md-7 col-xs-12'
													]
												);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product Name">Meta Tag Description<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'meta_description',[
														'rows'=>4,
														'label'=>false,
														'type'=>'textarea',
														'required'=>'required',
														'placeholder'=>'Meta Description',
														'value'=>$product['meta_description'],
														'class'=>'form-control col-md-7 col-xs-12'
													]
												);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product Name">Meta Tag Keywords<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'meta_keyword',[
														'value'=>$product['meta_keyword'],
														'type'=>'textarea',
														'required'=>'required',
														'rows'=>4,
														'class'=>'form-control col-md-7 col-xs-12',
														'label'=>false,
														'placeholder'=>'Meta Keywords']);?>
											</div>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
									<h2 class="text-center StepTitle">Product Data</h2>
									<div class="form-horizontal form-label-left">
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Model">Model<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'model',[
														'type'=>'text',
														'label'=>false,
														'placeholder'=>'Model',
														'required'=>'required',
														'value'=>$product['model'],
														'class'=>'form-control col-md-7 col-xs-12',
														]
												);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="SKU">SKU</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input('sku',['value'=>$product['sku'],'type'=>'text', 'class'=>'form-control col-md-7 col-xs-12','label'=>false,'placeholder'=>'SKU']);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="UPC">UPC</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input('upc',['value'=>$product['upc'],'type'=>'text',  'class'=>'form-control col-md-7 col-xs-12','label'=>false,'placeholder'=>'UPC']);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="EAN">EAN</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input('ean',['value'=>$product['ean'],'type'=>'text',  'class'=>'form-control col-md-7 col-xs-12','label'=>false,'placeholder'=>'EAN']);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="JAN">JAN</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input('jan',['value'=>$product['jan'],'type'=>'text',  'class'=>'form-control col-md-7 col-xs-12','label'=>false,'placeholder'=>'JAN']);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="ISNB">ISNB</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input('isbn',['value'=>$product['isbn'],'type'=>'text', 'class'=>'form-control col-md-7 col-xs-12','label'=>false,'placeholder'=>'ISBN']);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="MPN">Manufacture Part Number</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input('mpn',['value'=>$product['mpn'],'type'=>'text',  'class'=>'form-control col-md-7 col-xs-12','label'=>false,'placeholder'=>'MPN']);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="quantity">Quantity<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input('quantity',['value'=>$product['quantity'],'type'=>'text', 'required'=>'required', 'class'=>'form-control col-md-7 col-xs-12','label'=>false]);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input('status',['value'=>$product['status'],'type'=>'select', 'options'=>$status_list, 'class'=>'form-control col-md-7 col-xs-12','label'=>false]);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="shipping">Shipping Method</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input('shipping',['value'=>$product['shipping'],'type'=>'select', 'options'=>['free'=>'Free Postage','pick up'=>'Pick Up Only'], 'class'=>'form-control col-md-7 col-xs-12','label'=>false]);?>
											</div>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab2">
									<h2 class="text-center StepTitle">Pricing</h2>
									<div class="form-horizontal form-label-left">
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Price<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'price',[
														'type'=>'text',
														'label'=>false,
														'placeholder'=>'0.00',
														'required'=>'required',
														'value'=>$product['price'],
														'class'=>'form-control col-md-7 col-xs-12',
												]);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Price Per</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'pricing',[
														'label'=>false,
														'type'=>'select',
														'placeholder'=>'Price',

														'value'=>$product['pricing'],
														'class'=>'form-control col-md-7 col-xs-12',
														'options'=>['hour'=>'Hour','night'=>'Night','day'=>'Day','week'=>'Week','month'=>'Month'],
												]);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Late Fee<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'fine',[
														'type'=>'text',
														'label'=>false,
														'placeholder'=>'0.00',
														'required'=>'required',
														'value'=>$product['fine'],
														'class'=>'form-control col-md-7 col-xs-12',
												]);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Late Fee Per</label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'fine_per',[
														'label'=>false,
														'type'=>'select',

														'placeholder'=>'Price',
														'value'=>$product['fine_per'],
														'class'=>'form-control col-md-7 col-xs-12',
														'options'=>['hour'=>'Hour','night'=>'Night','day'=>'Day','week'=>'Week','month'=>'Month'],
												]);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Deposit<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'deposit',['value'=>$product['deposit'],
														'type'=>'text',
														'label'=>false,
														'placeholder'=>'0.00',
														'required'=>'required',
														'class'=>'form-control col-md-7 col-xs-12',
												]);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Special Offer</label>
											<div class="control-group col-sm-3 col-md-3 col-xs-12">
												<div class="controls">
													<div class="input-prepend input-group">
														<span class="add-on input-group-addon no-radius"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
														<input type="text" style="width:200px" name="offer_start_end_date" id="reservation" class="form-control" value="<?=date("d/m/Y")?> - <?=date("d/m/Y")?>" />
													</div>
												</div>
											</div>
											<div class="col-md-3 col-sm-3 col-xs-12">
												<div class="controls">
													<div class="input-prepend input-group">
														<?=$this->Form->input(
															'special_offer',[
																'label'=>false,
																'type'=>'text',
																'placeholder'=>'0.00',
																'value'=>$product['value'],
																'class'=>'form-control no-ragius col-md-7 col-xs-12',
														]);?>
														<span class="add-on no-radius input-group-addon"> <i class="text-danger"> % OFF </i></span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?=$this->Form->end();?>
				</div>
				<div class="clearfix"></div>
				<div class="x_panel">
					<div class="x_title row">
						<h2><?=$product['name']?> Images</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
					</div>
					<div class="form-horizontal form-label-left">
						<p class="text-center">Hover on the first Image to add new.</p>
						<div class="col-md-55">
							<div class="thumbnail no-radius">
								<div class="image view view-first">
									<img class="img-responsive" style="height: 100%; margin: auto; display: block;" src="/img/products/product.png" alt="image" />
									<div class="mask">
										<p>Add Another Image </p>
										<div class="tools tools-bottom">
											<a data-toggle="modal" data-target="#Add_Image"><i class="fa fa-plus"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php
						foreach($productImages as $image){?>
							<div class="col-md-55">
								<div class="thumbnail no-radius">
									<div class="image view view-first">
										<img class="img-responsive" style="height: 100%; margin: auto; display: block;" src="/img/products/<?=$image['name'];?>" alt="image" />
										<div class="mask">
											<div class="tools tools-bottom">
												<a href="#"><i class="fa fa-link"></i></a>
												<a href="#"><i class="fa fa-pencil"></i></a>
												<a href="#"><i class="fa fa-times"></i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div> <!-- Images Panel-->
			</div>
		</div>
		<!-- /page content -->

		<!-- footer content -->
		<footer>
			<div class="text-center">
				The Cam Connect
			</div>
			<div class="clearfix"></div>
		</footer>
		<!-- /footer content -->
	</div>

	<!-- jQuery -->
	<script src="/vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="/vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script src="/vendors/nprogress/nprogress.js"></script>
	<!-- Chart.js -->
	<script src="/vendors/Chart.js/dist/Chart.min.js"></script>
	<!-- gauge.js -->
	<script src="/vendors/gauge.js/dist/gauge.min.js"></script>
	<!-- bootstrap-progressbar -->
	<script src="/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	<!-- iCheck -->
	<script src="/vendors/iCheck/icheck.min.js"></script>
	<!-- Skycons -->
	<script src="/vendors/skycons/skycons.js"></script>
	<!-- Flot -->
	<script src="/vendors/Flot/jquery.flot.js"></script>
	<script src="/vendors/Flot/jquery.flot.pie.js"></script>
	<script src="/vendors/Flot/jquery.flot.time.js"></script>
	<script src="/vendors/Flot/jquery.flot.stack.js"></script>
	<script src="/vendors/Flot/jquery.flot.resize.js"></script>
	<!-- Flot plugins -->
	<script src="/js/flot/jquery.flot.orderBars.js"></script>
	<script src="/js/flot/date.js"></script>
	<script src="/js/flot/jquery.flot.spline.js"></script>
	<script src="/js/flot/curvedLines.js"></script>
	<!-- jVectorMap -->
	<script src="/js/maps/jquery-jvectormap-2.0.3.min.js"></script>
	<!-- bootstrap-daterangepicker -->
	<script src="/js/moment/moment.min.js"></script>
	<script src="/js/datepicker/daterangepicker.js"></script>

	<!-- Custom Theme Scripts -->
	<script src="/js/custom.min.js"></script>

	<!-- jVectorMap -->
	<script src="/js/maps/jquery-jvectormap-world-mill-en.js"></script>
	<script src="/js/maps/jquery-jvectormap-us-aea-en.js"></script>
	<script src="/js/maps/gdp-data.js"></script>
	<script src="/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
	<script src="/vendors/nprogress/nprogress.js"></script>
	<!-- Dropzone.js -->
	<script src="/vendors/dropzone/dist/min/dropzone.min.js"></script>

	<script>
		$(document).ready(function(){
			$('#world-map-gdp').vectorMap({
				map: 'world_mill_en',
				backgroundColor: 'transparent',
				zoomOnScroll: false,
				series: {
					regions: [{
						values: gdpData,
						scale: ['#E6F2F0', '#149B7E'],
						normalizeFunction: 'polynomial'
					}]
				},
				onRegionTipShow: function(e, el, code) {
					el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
				}
			});
		});
	</script>
	<!-- /jVectorMap -->

	<!-- Skycons -->
	<script>
		$(document).ready(function() {
			var icons = new Skycons({
						"color": "#73879C"
					}),
					list = [
						"clear-day", "clear-night", "partly-cloudy-day",
						"partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
						"fog"
					],
					i;

			for (i = list.length; i--;)
				icons.set(list[i], list[i]);
			icons.play();
		});
	</script>
	<!-- /Skycons -->
	<script>
		$(document).ready(function() {
			$('#reservation').daterangepicker(null, function(start, end, label) {
				console.log(start.toISOString(), end.toISOString(), label);
			});
		});
	</script>
	<!-- /bootstrap-daterangepicker -->

	<!-- jQuery Smart Wizard -->
	<script>
		$(document).ready(function() {
			$('#wizard').smartWizard();

			$('#wizard_verticle').smartWizard({
				transitionEffect: 'slide'
			});

			$('.buttonNext').addClass('btn btn-success');
			$('.buttonPrevious').addClass('btn btn-primary');
			$('.buttonFinish').addClass('btn btn-default');
		});
	</script>
	<!-- /jQuery Smart Wizard -->


	<!-- Modal -->
	<div class="modal fade" id="Add_Image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content  no-radius">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Add Image</h4>
				</div>
				<form class="form-horizontal" enctype="multipart/form-data" action="/Admin/editProduct/<?=$product['id'];?>" method="post">
					<div class="modal-body">
						<div class="padding row">
							<div class="row form-group padding-top-20">
								<div class="col-sm-12"><?= $this->Form->input('Image',['type'=>'file','name'=>'image']); ?></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="row">
							<div class="text-center"><button type="submit" name="upload_image" class="btn btn-sm no-radius btn-primary">Upload Image <i class="fa fa-upload"></i></button></div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
