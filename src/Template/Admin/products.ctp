<?php
$this->layout = false;
$active_nav = "products";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>The Cam Connect Dashboard </title>

	<!-- Bootstrap -->
	<link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- iCheck -->
	<link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	<!-- bootstrap-progressbar -->
	<link href="/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
	<!-- jVectorMap -->
	<link href="/css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>

	<!-- Custom Theme Style -->
	<link href="/css/custom.min.css" rel="stylesheet">
	<link href="/css/admin/custom.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
	<div class="main_container">
		<div class="col-md-3 left_col">
			<div class="left_col scroll-view">
				<div class="navbar nav_title" style="border: 0;">
					<a href="" class="site_title">
						<i class="fa fa-camera"></i>
						<span>TheCam<span class="text-warning small"><em>Connect</em></span></span>
					</a>
				</div>

				<div class="clearfix"></div>

				<!-- menu profile quick info -->
				<div class="profile">
					<div class="profile_pic">
						<img src="/img/img.jpg" alt="..." class="img-circle profile_img">
					</div>
					<div class="profile_info">
						<span>Welcome,</span>
						<h2><?=$user['firstname'] .' '. $user['lastname'];?></h2>
					</div>
				</div>
				<!-- /menu profile quick info -->

				<br/>

				<!-- sidebar menu -->
				<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
					<div class="menu_section">
						<h3>Administrator</h3>
						<ul class="nav side-menu">
							<li><a href="/Admin/"><i class="fa fa-dashboard"></i> Dashboard </span></a>
							<li><a><i class="fa fa-tags"></i> Categories <span
											class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/Categories">View Categories</a></li>
									<li><a href="/Admin/AddCategory">Add New Category</a></li>
								</ul>
							</li>
							<li class="active"><a><i class="fa fa-camera-retro"></i> Products <span class="fa fa-chevron-down pull-right"></span></a>
								<ul class="nav child_menu">
									<li><a class="active" href="/Admin/Products/">Products</a></li>
									<li><a href="/Admin/AddProduct">Add New Product</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-desktop"></i> Design <span
											class="fa fa-chevron-down pull-right"></span><span
											class="label label-success pull-right">Coming Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/Layout">Layout</a></li>
									<li><a href="/Admin/Banner">Banner</a></li>
									<li><a href="/Admin/Media">Media</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-shopping-cart"></i> Sales <span
											class="fa fa-chevron-down pull-right"></span><span
											class="label label-success pull-right">Coming Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/SalesOrders">Orders</a></li>
									<li><a href="/Admin/RecurringOrders">Recurring Orders</a></li>
									<li><a href="/Admin/SalesReturns">Returns</a></li>
									<li><a href="/Admin/GiftVouchers">Gift Vouchers</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-user"></i> Customers <span
											class="fa fa-chevron-down pull-right"></span><span
											class="label label-success pull-right">C.Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/Customers">Customers</a></li>
									<li><a href="/Admin/CustomerGroups">Customer Groups</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-bar-chart"></i>Report <span
											class="fa fa-chevron-down pull-right"></span><span
											class="label label-success pull-right">Coming Soon</span> </a>
								<ul class="nav child_menu">
									<li><a href="/Admin/SalesReport">Sales</a></li>
									<li><a href="/Admin/CustomersReport/">Customers</a></li>
									<li><a href="/Admin/MarketingReport/">Marketing</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-gears"></i>System <span class="fa fa-chevron-down pull-right"></span><span
											class="label label-success pull-right">Coming Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/SystemSettings/">Settings</a></li>
									<li><a href="/Admin/SystemUsers/">Users</a></li>
									<li><a href="/Admin/SystemTools/">Tools</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /sidebar menu -->

				<!-- /menu footer buttons -->
				<div class="sidebar-footer hidden-small">
					<a data-toggle="tooltip" data-placement="top" title="Settings">
						<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
					</a>
					<a data-toggle="tooltip" data-placement="top" title="FullScreen">
						<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
					</a>
					<a data-toggle="tooltip" data-placement="top" title="Lock">
						<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
					</a>
					<a data-toggle="tooltip" data-placement="top" title="Logout">
						<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
					</a>
				</div>
				<!-- /menu footer buttons -->
			</div>
		</div>

		<!-- top navigation -->
		<div class="top_nav">
			<div class="nav_menu">
				<nav>
					<div class="nav toggle">
						<a id="menu_toggle"><i class="fa fa-bars"></i></a>
					</div>

					<ul class="nav navbar-nav navbar-right">
						<li class="">
							<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
							   aria-expanded="false">
								<img src="/img/img.jpg" alt=""><?=$user['firstname'] .' '. $user['lastname'];?>
								<span class=" fa fa-angle-down"></span>
							</a>
							<ul class="dropdown-menu dropdown-usermenu pull-right">
								<li><a href="javascript:;"> Profile</a></li>
								<li>
									<a href="javascript:;">
										<span class="badge bg-red pull-right">50%</span>
										<span>Settings</span>
									</a>
								</li>
								<li><a href="javascript:;">Help</a></li>
								<li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
							</ul>
						</li>

						<li role="presentation" class="dropdown">
							<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
							   aria-expanded="false">
								<i class="fa fa-envelope-o"></i>
								<span class="badge bg-green">6</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="row">
				<div class="x_panel">
					<h3><i class="fa fa-fw fa-camera-retro"></i> Products</h3>
				</div>
				<div class="text-center"><?=$this->Flash->render();?></div>

				<div class="x_panel">
					<div class="x_title">
						<h2>Categories
							<small>[showing all categories]</small>
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li class="pull-right">
								<a class="btn btn-default btn-xs no-radius" href="/Admin/add_product">
									<i class="fa fa-plus"></i></a></li>
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<table class="table table-bordered table-responsive table-striped">
							<thead>
							<tr>
								<th class="text-center">image</th>
								<th class="text-center">Product Name</th>
								<th class="text-center">Model</th>
								<th class="text-center">Price</th>
								<th class="text-center">Stock Level</th>
								<th class="text-center">Status</th>
								<th class="text-center">Action</th>
							</tr>
							</thead>
							<tbody>
								<?php
									isset($_GET['page']) ? $page = $_GET['page'] : $page = 0;
									if(!empty($products[$page])){
										foreach($products[$page] as $product){?>
											<tr>
												<td width="50">
													<?php if(!empty($product['images'])){?>
														<img class="img-responsive" src="/img/products/<?=$product['images'][0]['name']?>" alt="..." />
													<?php } else {?>
														<img class="img-responsive" src="/img/products/product.png" alt="..." />
													<?php } ?>
												</td>
												<td class="padding-top-20 text-center"><?=$product['name']?></td>
												<td class="padding-top-20 text-center"><?=$product['model']?></td>
												<td class="padding-top-20 text-center">£<?=$product['price']?> <small> Per </small> <?=$product['price_per']?></td>
												<td class="padding-top-20 text-center" width="100"><?=$product['quantity']?></td>
												<td class="padding-top-20 text-center" width="100"><?=$product['status']?></td>
												<td class="padding-top-10 text-center" width="100">
													<a class="btn btn-xs no-radius btn-default" href="/Admin/product/<?=$product['id']?>"><i class="fa fa-fw fa-folder-open-o"></i></a>
													<a class="btn btn-xs no-radius btn-warning" href="/Admin/editProduct/<?=$product['id']?>"> <i class="fa fa-fw fa-pencil"></i></a>
												</td>
											</tr>
								<?php } }else{ ?>
											<tr><td colspan="8"><h3 class="text-danger text-center">No Products On this Page </h3></td></tr>
								<?php } ?>
							</tbody>
						</table>
						<div class="row">

							<div class="col-sm-4 col-xs-12 text-center">
								<?php if($page > 0){ ?>
									<a href="/Admin/Products?page=<?=$page-1;?>" class="btn btn-success btn-xs btn-rounded"><i class="fa fa-arrow-left"></i> Previous </a>
								<?php } ?>
							</div>
							<div class="col-sm-4 col-xs-12 text-center">
								<small>Page <?=$page+1?> of <?=count($products);?></small>
							</div>
							<div class="col-sm-4 col-xs-12 text-center">
								<?php if(!empty($products[$page+1])){?>
									<a href="/Admin/Products?page=<?=$page+1;?>" class="btn btn-primary btn-xs btn-rounded" style="margin-right: 5px;">Next <i class="fa fa-arrow-right"></i></a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /page content -->

		<!-- footer content -->
		<footer>
			<div class="text-center">
				The Cam Connect
			</div>
			<div class="clearfix"></div>
		</footer>
		<!-- /footer content -->
	</div>
</div>

<!-- jQuery -->
<script src="/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="/vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="/vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="/vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="/vendors/Flot/jquery.flot.js"></script>
<script src="/vendors/Flot/jquery.flot.pie.js"></script>
<script src="/vendors/Flot/jquery.flot.time.js"></script>
<script src="/vendors/Flot/jquery.flot.stack.js"></script>
<script src="/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="/js/flot/jquery.flot.orderBars.js"></script>
<script src="/js/flot/date.js"></script>
<script src="/js/flot/jquery.flot.spline.js"></script>
<script src="/js/flot/curvedLines.js"></script>
<!-- jVectorMap -->
<script src="/js/maps/jquery-jvectormap-2.0.3.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="/js/moment/moment.min.js"></script>
<script src="/js/datepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="/js/custom.min.js"></script>

<!-- jVectorMap -->
<script src="/js/maps/jquery-jvectormap-world-mill-en.js"></script>
<script src="/js/maps/jquery-jvectormap-us-aea-en.js"></script>
<script src="/js/maps/gdp-data.js"></script>
<script>
	$(document).ready(function () {
		$('#world-map-gdp').vectorMap({
			map: 'world_mill_en',
			backgroundColor: 'transparent',
			zoomOnScroll: false,
			series: {
				regions: [{
					values: gdpData,
					scale: ['#E6F2F0', '#149B7E'],
					normalizeFunction: 'polynomial'
				}]
			},
			onRegionTipShow: function (e, el, code) {
				el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
			}
		});
	});
</script>
<!-- /jVectorMap -->

<!-- Skycons -->
<script>
	$(document).ready(function () {
		var icons = new Skycons({
					"color": "#73879C"
				}),
				list = [
					"clear-day", "clear-night", "partly-cloudy-day",
					"partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
					"fog"
				],
				i;

		for (i = list.length; i--;)
			icons.set(list[i], list[i]);
		icons.play();
	});
</script>
<!-- /Skycons -->

<!-- gauge.js -->
<script>
	var opts = {
		lines: 12,
		angle: 0,
		lineWidth: 0.4,
		pointer: {
			length: 0.75,
			strokeWidth: 0.042,
			color: '#1D212A'
		},
		limitMax: 'false',
		colorStart: '#1ABC9C',
		colorStop: '#1ABC9C',
		strokeColor: '#F0F3F3',
		generateGradient: true
	};
	var target = document.getElementById('foo'),
			gauge = new Gauge(target).setOptions(opts);

	gauge.maxValue = 6000;
	gauge.animationSpeed = 32;
	gauge.set(3200);
	gauge.setTextField(document.getElementById("gauge-text"));
</script>
<!-- /gauge.js -->
</body>
</html>
