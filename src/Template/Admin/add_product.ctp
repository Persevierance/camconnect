<?php
$this->layout = false;
$active_nav = "dashboard";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>The Cam Connect Dashboard </title>

	<!-- Bootstrap -->
	<link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- iCheck -->
	<link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	<!-- bootstrap-progressbar -->
	<link href="/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
	<link href="/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
	<!-- jVectorMap -->
	<link href="/css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>

	<!-- Custom Theme Style -->
	<link href="/css/custom.min.css" rel="stylesheet">
	<link href="/css/admin/custom.css" rel="stylesheet">

	<!-- Dropzone.js -->
	<link href="/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">



</head>

<body class="nav-md">
<div class="container body">
	<div class="main_container">
		<div class="col-md-3 left_col">
			<div class="left_col scroll-view">
				<div class="navbar nav_title" style="border: 0;">
					<a href="" class="site_title">
						<i class="fa fa-camera"></i>
						<span>TheCam<span class="text-warning small"><em>Connect</em></span></span>
					</a>
				</div>

				<div class="clearfix"></div>

				<!-- menu profile quick info -->
				<div class="profile">
					<div class="profile_pic">
						<?=$this->Html->image('img.jpg',['alt'=>'Profile Image','class'=>'img-circle profile_img']);?>
					</div>
					<div class="profile_info">
						<span>Welcome,</span>
						<h2><?=$user['firstname'] .' '. $user['lastname'];?></h2>
					</div>
				</div>
				<!-- /menu profile quick info -->

				<br />

				<!-- sidebar menu -->
				<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
					<div class="menu_section">
						<h3>Administrator</h3>
						<ul class="nav side-menu">
							<li><a href="/Admin/"><i class="fa fa-dashboard"></i> Dashboard </span></a>
							<li><a><i class="fa fa-tags"></i> Categories <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li><a  href="/Admin/Categories">View Categories</a></li>
									<li><a href="/Admin/AddCategory">Add New Category</a></li>
								</ul>
							</li>
							<li class="active"><a><i class="fa fa-camera-retro"></i> Products <span class="fa fa-chevron-down pull-right"></span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/Products/">Products</a></li>
									<li><a class="active" href="/Admin/AddProduct">Add New Product</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-desktop"></i> Design <span class="fa fa-chevron-down pull-right"></span><span class="label label-success pull-right">Coming Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/Layout">Layout</a></li>
									<li><a href="/Admin/Banner">Banner</a></li>
									<li><a href="/Admin/Media">Media</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-shopping-cart"></i> Sales <span class="fa fa-chevron-down pull-right"></span><span class="label label-success pull-right">Coming Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/SalesOrders">Orders</a></li>
									<li><a href="/Admin/RecurringOrders">Recurring Orders</a></li>
									<li><a href="/Admin/SalesReturns">Returns</a></li>
									<li><a href="/Admin/GiftVouchers">Gift Vouchers</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-user"></i> Customers <span class="fa fa-chevron-down pull-right"></span><span class="label label-success pull-right">C.Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/Customers">Customers</a></li>
									<li><a href="/Admin/CustomerGroups">Customer Groups</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-bar-chart"></i>Report <span class="fa fa-chevron-down pull-right"></span><span class="label label-success pull-right">Coming Soon</span> </a>
								<ul class="nav child_menu">
									<li><a href="/Admin/SalesReport">Sales</a></li>
									<li><a href="/Admin/CustomersReport/">Customers</a></li>
									<li><a href="/Admin/MarketingReport/">Marketing</a></li>
								</ul>
							</li>
							<li><a><i class="fa fa-gears"></i>System <span class="fa fa-chevron-down pull-right"></span><span class="label label-success pull-right">Coming Soon</span></a>
								<ul class="nav child_menu">
									<li><a href="/Admin/SystemSettings/">Settings</a></li>
									<li><a href="/Admin/SystemUsers/">Users</a></li>
									<li><a href="/Admin/SystemTools/">Tools</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /sidebar menu -->

				<!-- /menu footer buttons -->
				<div class="sidebar-footer hidden-small">
					<a data-toggle="tooltip" data-placement="top" title="Settings">
						<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
					</a>
					<a data-toggle="tooltip" data-placement="top" title="FullScreen">
						<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
					</a>
					<a data-toggle="tooltip" data-placement="top" title="Lock">
						<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
					</a>
					<a data-toggle="tooltip" data-placement="top" title="Logout">
						<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
					</a>
				</div>
				<!-- /menu footer buttons -->
			</div>
		</div>

		<!-- top navigation -->
		<div class="top_nav">
			<div class="nav_menu">
				<nav>
					<div class="nav toggle">
						<a id="menu_toggle"><i class="fa fa-bars"></i></a>
					</div>
					<ul class="nav navbar-nav navbar-right">
						<li class="">
							<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<?=$this->Html->image('img.jpg',['alt'=>'']);?><?=$user['firstname'] .' '. $user['lastname'];?>
								<span class=" fa fa-angle-down"></span>
							</a>
							<ul class="dropdown-menu dropdown-usermenu pull-right">
								<li><a href="javascript:;"> Profile</a></li>
								<li>
									<a href="javascript:;">
										<span class="badge bg-red pull-right">50%</span>
										<span>Settings</span>
									</a>
								</li>
								<li><a href="javascript:;">Help</a></li>
								<li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
							</ul>
						</li>

						<li role="presentation" class="dropdown">
							<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
								<i class="fa fa-envelope-o"></i>
								<span class="badge bg-green">6</span>
							</a>
							<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
								<li>
									<a>
										<span class="image"><?=$this->Html->image('img.jpg',['alt'=>'Profile Image']);?></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
									</a>
								</li>
								<li>
									<a>
										<span class="image"><?=$this->Html->image('img.jpg',['alt'=>'Profile Image']);?></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
									</a>
								</li>
								<li>
									<a>
										<span class="image"><?=$this->Html->image('img.jpg',['alt'=>'Profile Image']);?></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
									</a>
								</li>
								<li>
									<a>
										<span class="image"><?=$this->Html->image('img.jpg',['alt'=>'Profile Image']);?></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
									</a>
								</li>
								<li>
									<div class="text-center">
										<a>
											<strong>See All Alerts</strong>
											<i class="fa fa-angle-right"></i>
										</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="row">
				<div class="x_panel">
					<h3><i class="fa fa-fw fa-tags"></i>Add Product</h3>
				</div>

				<div class="text-center"><?= $this->Flash->render();?></div>

				<div class="x_panel">
					<?=$this->Form->create();?>
						<div class="x_title">
							<h2>Add New <small>Product</small></h2>
							<ul class="nav navbar-right panel_toolbox">
								<li class=""><button class="btn btn-primary btn-xs"><i class="fa fa-check"></i> Save</button></li>
<!--							<li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>-->
							</ul>
							<div class="clearfix"></div>
						</div>

						<div class="" role="tabpanel" data-example-id="togglable-tabs">
							<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
								<li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">General </a></li>
								<li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Data </a></li>
								<li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Pricing</a></li>
							</ul>
							<div id="myTabContent" class="tab-content">
								<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
									<h2 class="text-center StepTitle">General Product Information</h2>
									<div class='form-horizontal form-label-left'>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product Name">Product Name<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'name',[
														'type'=>'text',
														'label'=>false,
														'required'=>'required',
														'placeholder'=>'Product Name',
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product Name">Product Description<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'description',[
														'rows'=>4,
														'label'=>false,
														'type'=>'textarea',
														'required'=>'required',
														'placeholder'=>'Product Description',
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product Name">Product Category<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'category_id',[
														'label'=>false,
														'type'=>'select',
														'required'=>'required',
														'options'=>$categories,
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Model">Model<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'model',[
														'type'=>'text',
														'label'=>false,
														'placeholder'=>'Model',
														'required'=>'required',
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>


										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product Name">Meta Tag Title<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'meta_title',[
														'type'=>'text',
														'label'=>false,
														'required'=>'required',
														'placeholder'=>'Meta Tag Title',
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product Name">Meta Tag Description<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'meta_description',[
														'rows'=>4,
														'label'=>false,
														'type'=>'textarea',
														'required'=>'required',
														'placeholder'=>'Meta Description',
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product Name">Meta Tag Keywords<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'meta_keyword',[
														'rows'=>4,
														'label'=>false,
														'required'=>'required',
														'placeholder'=>'Meta Keywords',
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>
										</div>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
									<h2 class="text-center StepTitle">Product Data</h2>
									<div class="form-horizontal form-label-left">
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="SKU">SKU<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'sku',[
														'type'=>'text',
														'label'=>false,
														'placeholder'=>'SKU',
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="UPC">UPC<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'upc',[
														'type'=>'text',
														'label'=>false,
														'placeholder'=>'UPC',
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="EAN">EAN<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'ean',[
														'type'=>'text',
														'label'=>false,
														'placeholder'=>'EAN',
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="JAN">JAN<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'jan',[
														'type'=>'text',
														'label'=>false,
														'placeholder'=>'JAN',
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="ISNB">ISNB<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'isbn',[
														'type'=>'text',
														'label'=>false,
														'placeholder'=>'ISBN',
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="MPN">Manufacture Part Number<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'mpn',[
														'type'=>'text',
														'label'=>false,
														'placeholder'=>'MPN',
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="quantity">quantity<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'quantity',[
														'type'=>'text',
														'label'=>false,
														'class'=>'form-control col-md-7 col-xs-12',
												]);?>
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'status',[
														'value'=>1,
														'label'=>false,
														'type'=>'select',
														'options'=>$status_list,
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="shipping">Shipping Method<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'shipping',[
														'label'=>false,
														'type'=>'select',
														'options'=>[
															'free'=>'Free Postage',
																'pick up'=>'Pick Up Only'
														],
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab2">
									<h2 class="text-center StepTitle">Pricing</h2>
									<div class="form-horizontal form-label-left">
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Price<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'price',[
														'label'=>false,
														'type'=>'text',
														'placeholder'=>'0.00',
														'required'=>'required',
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Price Per<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'pricing',[
														'type'=>'select',
														'label'=>false,
														'placeholder'=>'0.00',
														'options'=>[
															'day'=>'Day',
															'hour'=>'Hour',
															'week'=>'Week',
															'month'=>'Month',
															'night'=>'Night',
														], 'required'=>'required',
														'class'=>'form-control col-md-7 col-xs-12',
												]);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Late Fee<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'fine',[
														'type'=>'text',
														'label'=>false,
														'placeholder'=>'0.00',
														'class'=>'form-control col-md-7 col-xs-12',
													]);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Late Fee Per<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'fine_per',[
														'type'=>'select',
														'label'=>false,
														'options'=>[
															'hour'=>'Hour',
															'night'=>'Night',
															'day'=>'Day',
															'week'=>'Week',
															'month'=>'Month'
														],'placeholder'=>'Price',
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Deposit<span class="required">*</span></label>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<?=$this->Form->input(
													'deposit',[
														'type'=>'text',
														'label'=>false,
														'placeholder'=>'0.00',
														'class'=>'form-control col-md-7 col-xs-12',
													]
												);?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Special Offer<span class="required">*</span></label>
											<div class="control-group col-sm-3 col-md-3 col-xs-12">
												<div class="controls">
													<div class="input-prepend input-group">
														<span class="add-on input-group-addon no-radius"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
														<input type="text" style="width:200px" name="offer_start_end_date" id="reservation" class="form-control " value="<?=date("d/m/Y")?> - <?=date("d/m/Y")?>" />
													</div>
												</div>
											</div>
											<div class="col-md-3 col-sm-3 col-xs-12">
												<div class="controls">
													<div class="input-prepend input-group">
														<?=$this->Form->input(
															'special_offer',[
																'type'=>'text',
																'label'=>false,
																'placeholder'=>'0.00',
																'class'=>'form-control no-ragius col-md-7 col-xs-12',
															]
														);?>
														<span class="add-on no-radius input-group-addon"> <i class="text-danger"> % OFF </i></span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?=$this->Form->end();?>
				</div>
			</div>
		</div>
		<!-- /page content -->

		<!-- footer content -->
		<footer>
			<div class="text-center">
				The Cam Connect
			</div>
			<div class="clearfix"></div>
		</footer>
		<!-- /footer content -->
	</div>

	<!-- jQuery -->
	<script src="/vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="/vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script src="/vendors/nprogress/nprogress.js"></script>
	<!-- Chart.js -->
	<script src="/vendors/Chart.js/dist/Chart.min.js"></script>
	<!-- gauge.js -->
	<script src="/vendors/gauge.js/dist/gauge.min.js"></script>
	<!-- bootstrap-progressbar -->
	<script src="/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	<!-- iCheck -->
	<script src="/vendors/iCheck/icheck.min.js"></script>
	<!-- Skycons -->
	<script src="/vendors/skycons/skycons.js"></script>
	<!-- Flot -->
	<script src="/vendors/Flot/jquery.flot.js"></script>
	<script src="/vendors/Flot/jquery.flot.pie.js"></script>
	<script src="/vendors/Flot/jquery.flot.time.js"></script>
	<script src="/vendors/Flot/jquery.flot.stack.js"></script>
	<script src="/vendors/Flot/jquery.flot.resize.js"></script>
	<!-- Flot plugins -->
	<script src="/js/flot/jquery.flot.orderBars.js"></script>
	<script src="/js/flot/date.js"></script>
	<script src="/js/flot/jquery.flot.spline.js"></script>
	<script src="/js/flot/curvedLines.js"></script>
	<!-- jVectorMap -->
	<script src="/js/maps/jquery-jvectormap-2.0.3.min.js"></script>
	<!-- bootstrap-daterangepicker -->
	<script src="/js/moment/moment.min.js"></script>
	<script src="/js/datepicker/daterangepicker.js"></script>

	<!-- Custom Theme Scripts -->
	<script src="/js/custom.min.js"></script>

	<!-- jVectorMap -->
	<script src="/js/maps/jquery-jvectormap-world-mill-en.js"></script>
	<script src="/js/maps/jquery-jvectormap-us-aea-en.js"></script>
	<script src="/js/maps/gdp-data.js"></script>
	<script src="/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
	<script src="/vendors/nprogress/nprogress.js"></script>
	<!-- Dropzone.js -->
	<script src="/vendors/dropzone/dist/min/dropzone.min.js"></script>

	<script>
		$(document).ready(function(){
			$('#world-map-gdp').vectorMap({
				map: 'world_mill_en',
				backgroundColor: 'transparent',
				zoomOnScroll: false,
				series: {
					regions: [{
						values: gdpData,
						scale: ['#E6F2F0', '#149B7E'],
						normalizeFunction: 'polynomial'
					}]
				},
				onRegionTipShow: function(e, el, code) {
					el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
				}
			});
		});
	</script>
	<!-- /jVectorMap -->

	<!-- Skycons -->
	<script>
		$(document).ready(function() {
			var icons = new Skycons({
						"color": "#73879C"
					}),
					list = [
						"clear-day", "clear-night", "partly-cloudy-day",
						"partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
						"fog"
					],
					i;

			for (i = list.length; i--;)
				icons.set(list[i], list[i]);
			icons.play();
		});
	</script>
	<!-- /Skycons -->
	<script>
		$(document).ready(function() {
			$('#reservation').daterangepicker(null, function(start, end, label) {
				console.log(start.toISOString(), end.toISOString(), label);
			});
		});
	</script>
	<!-- /bootstrap-daterangepicker -->

	<!-- jQuery Smart Wizard -->
	<script>
		$(document).ready(function() {
			$('#wizard').smartWizard();

			$('#wizard_verticle').smartWizard({
				transitionEffect: 'slide'
			});

			$('.buttonNext').addClass('btn btn-success');
			$('.buttonPrevious').addClass('btn btn-primary');
			$('.buttonFinish').addClass('btn btn-default');
		});
	</script>
	<!-- /jQuery Smart Wizard -->
</body>
</html>
