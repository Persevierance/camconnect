<?php
$this->layout = false;
$pageTitle = 'TheCamConnect | Products';
?>
<!DOCTYPE html>
<html lang="en">
	<?=$this->element('Go/head');?>
	<div class="single single-product woocommerce woocommerce-page">

		<!-- HEADER -->
		<header id="masthead" class="clearfix" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
			<?=$this->element('Go/site-header');?>
			<?=$this->element('Go/top-nav');?>
		</header>
		<!-- / HEADER -->

		<!-- CONTENT + SIDEBAR -->
		<div class="main-wrapper clearfix">
			<div class="theme-container container">
				<div class="login_content">
					<div class="login-wrap text-center">
						<div class="fancy-heading text-center">
							<h3><span class="thm-clr">SIGN </span> UP</h3>
						</div>
						<div class="col-sm-10 col-sm-offset-1">
							<div class="text-center"><?= $this->Flash->render('auth');?></div>
							<?=$this->Form->create('register');?>
								<div class="form-group col-xs-12 col-sm-6"><?= $this->Form->input('firstname',['type'=>"text",'label'=>false, 'class'=>"form-control", 'placeholder'=>"First Name", 'required'=>"required"]);?></div>
								<div class="form-group col-xs-12 col-sm-6"><?= $this->Form->input('lastname',['type'=>"text",'label'=>false, 'class'=>"form-control", 'placeholder'=>"Last Name", 'required'=>"required"]);?></div>
								<div class="form-group col-xs-12 col-sm-6"><?= $this->Form->input('username',['type'=>"text",'label'=>false, 'class'=>"form-control", 'placeholder'=>"Email Address", 'required'=>"required"]);?></div>
								<div class="form-group col-xs-12 col-sm-6"><?= $this->Form->input('username',['type'=>"text",'label'=>false, 'class'=>"form-control", 'placeholder'=>"Email Address", 'required'=>"required"]);?></div>
								<div class="form-group col-xs-12 col-sm-6"><?= $this->Form->input('password',['type'=>"password",'label'=>false, 'class'=>"form-control", 'placeholder'=>"Password", 'required'=>"required"]);?></div>
								<div class="form-group col-xs-12 col-sm-6"><?= $this->Form->input('confirm_password',['type'=>"password",'label'=>false, 'class'=>"form-control", 'placeholder'=>"Confirm Password", 'required'=>"required"]);?></div>
								<div class="form-group col-xs-12 col-sm-4 col-sm-offset-4"><hr /></div>
								<div class="form-group col-xs-12 col-sm-4 col-sm-offset-4"><?= $this->Form->button('Create Account',['type'=>"submit",'label'=>false, 'class'=>"alt btn-block fancy-button"]);?></div>
								<div class="form-group col-sm-6 col-xs-12"> <a class="fb-btn btn spcbtm-15" href="#"> <i class="fa fa-facebook btn-icon"></i>Register with Facebook</a></div>
								<div class="form-group col-sm-6 col-xs-12"> <a class="ggle-btn btn spcbtm-15" href="#"><i class="fa fa-google-plus btn-icon"></i>Register with Google Plus</a></div>
							<?= $this->Form->end();?>
							<p class="bold-font-2 fsz-12 signup"> OR <a href="/users/login">SIGN UP</a> </p>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<>
		<?=$this->element('Go/newsletter');?>
		<?=$this->element('Go/footer');?>
		<?=$this->element('Go/search-modal');?>
		<?=$this->element('Go/login-modal');?>

		<!-- Top -->
		<div class="to-top" id="to-top"><i class="fa fa-long-arrow-up"></i></div>

	</>

	<!-- JS Global -->
	<script src="/plugins/jquery/jquery-2.1.3.js"></script>
	<script src="/plugins/royalslider/jquery.royalslider.min.js"></script>
	<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="/plugins/bootstrap-select-1.9.3/dist/js/bootstrap-select.min.js"></script>
	<script src="/plugins/owl-carousel2/owl.carousel.min.js"></script>
	<script src="/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="/plugins/isotope-master/dist/isotope.pkgd.min.js"></script>

	<!-- Page JS -->
	<script src="/js/jquery.sticky.js"></script>
	<script src="/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="/js/custom.js"></script>
	<script>
		$('.datepicker').datepicker({
			maxViewMode: 1,
			orientation: "bottom left",
			toggleActive: true,
			autoclose: true
		});
	</script>
</body>
</html>
