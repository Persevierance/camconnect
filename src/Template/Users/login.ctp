<?php
$this->layout = false;
$pageTitle = 'TheCamConnect | Products';
?>
<!DOCTYPE html>
<html lang="en">
<?=$this->element('Go/head');?>
<body class="single single-product woocommerce woocommerce-page">

<!-- HEADER -->
<header id="masthead" class="clearfix" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
	<?=$this->element('Go/site-header');?>
	<?=$this->element('Go/top-nav');?>
</header>
<!-- / HEADER -->

<!-- CONTENT + SIDEBAR -->
<div class="main-wrapper clearfix">
	<div class="clearfix"></div>
	<div class="theme-container container">
		<div class="login_content">
			<div class="login-wrap text-center">
				<div class="fancy-heading text-center">
					<h3><span class="thm-clr">Account </span> Login</h3>
				</div>
				<div class="login-form">
					<div class="text-center"><?= $this->Flash->render('auth');?></div>
					<?=$this->Form->create('login',['class'=>'login','method'=>'post']);?>
						<div class="form-group"><input type="text" name="username" placeholder="Email" class="form-control"></div>
						<div class="form-group"><input type="password" name="password" placeholder="Password" class="form-control"></div>
						<div class="form-group">
							<button class="alt fancy-button" type="submit"><span class="fa fa-lightbulb-o"></span> Login</button>
						</div>
					<?=$this->Form->end();?>
					<a class="fb-btn btn spcbtm-15" href="#"> <i class="fa fa-facebook btn-icon"></i>Login with Facebook</a>
					<p class="bold-font-2 fsz-12 signup"> OR <a href="/users/register"> SIGN UP</a></p>
				</div>
			</div>
		</div>
	</div>
</div>

<?=$this->element('Go/newsletter');?>
<?=$this->element('Go/footer');?>
<?=$this->element('Go/search-modal');?>
<?=$this->element('Go/login-modal');?>
<!-- Top -->
<div class="to-top" id="to-top"><i class="fa fa-long-arrow-up"></i></div>
<!-- JS Global -->
<script src="/plugins/jquery/jquery-2.1.3.js"></script>
<script src="/plugins/royalslider/jquery.royalslider.min.js"></script>
<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/plugins/bootstrap-select-1.9.3/dist/js/bootstrap-select.min.js"></script>
<script src="/plugins/owl-carousel2/owl.carousel.min.js"></script>
<script src="/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/plugins/isotope-master/dist/isotope.pkgd.min.js"></script>

<!-- Page JS -->
<script src="/js/jquery.sticky.js"></script>
<script src="/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/js/custom.js"></script>
<script>
	$('.datepicker').datepicker({
		maxViewMode: 1,
		orientation: "bottom left",
		toggleActive: true,
		autoclose: true
	});
</script>
</body>
</html>
