<?php
	$this->layout = false;
	$pageTitle = 'TheCamConnect | ' . $user['firstname'] . ' Checkout';
?>
<!DOCTYPE html>
<html lang="en">
<?=$this->element('Go/head');?>
<body class="blog">
<!-- HEADER -->
<header id="masthead" class="clearfix" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
	<?=$this->element('Go/site-header')?>
	<?=$this->element('Go/top-nav');?>
</header>
<!-- / HEADER -->

<!-- CONTENT + SIDEBAR -->
<div class="main-wrapper clearfix">
	<div class="site-pagetitle jumbotron">
		<div class="container  theme-container text-center">
			<h3>My Order</h3>
			<!-- Breadcrumbs -->
			<div class="breadcrumbs">
				<div class="breadcrumbs text-center">
					<i class="fa fa-home"></i>
					<span><a href="/">Home</a></span>
					<i class="fa fa-arrow-circle-right"></i>
					<span class="current"> Checkout </span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container theme-container">
		<main id="main-content" class="main-container" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/Blog">
			<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost">

				<!-- Main Content of the Post -->
				<div class="entry-content" itemprop="articleBody">
					<div class="woocommerce checkout">
						<form name="checkout" method="post" class="checkout woocommerce-checkout" action="http://localhost/goshopwp/checkout/" enctype="multipart/form-data">
							<div class="col2-set clearfix" id="customer_details">
								<div class="col-1 col-lg-6 col-sm-6 border">
									<h4 class="cart-title-highlight title-3">Billing Details</h4>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group"><input class="form-control" type="text" placeholder="First Name"></div>
										</div>
										<div class="col-md-6">
											<div class="form-group"><input class="form-control" type="text" placeholder="Last Name"></div>
										</div>
										<div class="col-md-12">
											<div class="form-group"><input class="form-control" type="text" placeholder="Company Name"></div>
										</div>
										<div class="col-md-12">
											<div class="form-group"><input class="form-control" type="text" placeholder="Email"></div>
										</div>
										<div class="col-md-12">
											<div class="form-group"><input class="form-control" type="text" placeholder="Address"></div>
										</div>
										<div class="col-md-6">
											<div class="form-group selectpicker-wrapper">
												<select
														class="selectpicker input-price" data-live-search="true" data-width="100%"
														data-toggle="tooltip" title="Country">
													<option>Åland Islands</option>
													<option>Afghanistan</option>
													<option>Albania</option>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group selectpicker-wrapper">
												<select
														class="selectpicker input-price" data-live-search="true" data-width="100%"
														data-toggle="tooltip" title="Town / City">
													<option>Tirana</option>
													<option>Durres</option>
													<option>Vlore</option>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group"><input class="form-control" type="text" placeholder="Post Code"></div>
										</div>
										<div class="col-md-6">
											<div class="form-group"><input class="form-control" type="text" placeholder="Phone Number"></div>
										</div>
										<div class="col-md-12">
											<div class="form-group"><textarea class="form-control" placeholder="Addıtıonal Informatıon" name="name" cols="30" rows="5"></textarea></div>
										</div>
									</div>
								</div>

								<div class="col-2 col-lg-6 col-sm-6 border">
									<div class="woocommerce-shipping-fields">
										<h4 class="cart-title-highlight title-3">Ship to a different address?</h4>

										<div class="row">
											<div class="col-md-6">
												<div class="form-group"><input class="form-control" type="text" placeholder="First Name"></div>
											</div>
											<div class="col-md-6">
												<div class="form-group"><input class="form-control" type="text" placeholder="Last Name"></div>
											</div>
											<div class="col-md-12">
												<div class="form-group"><input class="form-control" type="text" placeholder="Company Name"></div>
											</div>
											<div class="col-md-12">
												<div class="form-group"><input class="form-control" type="text" placeholder="Email"></div>
											</div>
											<div class="col-md-12">
												<div class="form-group"><input class="form-control" type="text" placeholder="Address"></div>
											</div>
											<div class="col-md-6">
												<div class="form-group selectpicker-wrapper">
													<select
															class="selectpicker input-price" data-live-search="true" data-width="100%"
															data-toggle="tooltip" title="Country">
														<option>Åland Islands</option>
														<option>Afghanistan</option>
														<option>Albania</option>
													</select>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group selectpicker-wrapper">
													<select
															class="selectpicker input-price" data-live-search="true" data-width="100%"
															data-toggle="tooltip" title="Town / City">
														<option>Tirana</option>
														<option>Durres</option>
														<option>Vlore</option>
													</select>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group"><input class="form-control" type="text" placeholder="Post Code"></div>
											</div>
											<div class="col-md-6">
												<div class="form-group"><input class="form-control" type="text" placeholder="Phone Number"></div>
											</div>
											<div class="col-md-12">
												<div class="form-group"><textarea class="form-control" placeholder="Addıtıonal Informatıon" name="name" cols="30" rows="5"></textarea></div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div id="order_review" class="woocommerce-checkout-review-order ">
								<div class="col-lg-6 col-sm-6 border">
									<div class="chck-ttl">
										<h4 id="order_review_heading" class="cart-title-highlight title-3">Your order</h4>
										<table class="shop_table woocommerce-checkout-review-order-table">
											<thead>
											<tr>
												<th class="product-name">Product</th>
												<th class="product-total">Total</th>
											</tr>
											</thead>
											<tbody>
											<tr class="cart_item">
												<td class="product-name">Women ‘s Summer Dress <strong class="product-quantity">&times; 2</strong></td>
												<td class="product-total"><b class="amount">$150.00</b></td>
											</tr>
											<tr class="cart_item">
												<td class="product-name">Autum Winter Dress <strong class="product-quantity">&times; 1</strong></td>
												<td class="product-total"><b class="amount">$120.00</b></td>
											</tr>
											<tr class="cart_item">
												<td class="product-name">Flusas Feminin Dress <strong class="product-quantity">&times; 1</strong></td>
												<td class="product-total"><b class="amount">$75.00</b></td>
											</tr>
											</tbody>
											<tfoot>
											<tr>
												<th>Sub Total:</th>
												<td><span class="drk-gry">$130.00</span></td>
											</tr>

											<tr class="cart-discount">
												<th>Shipping Charge :</th>
												<td><b class="drk-gry">Free Shipping</b></td>
											</tr>
											<tr class="shipping">
												<th>Promo Discount :</th>
												<td>
													<b class="drk-gry">50%</b>
												</td>
											</tr>
											<tr class="order-total">
												<th>Order Total</th>
												<td><b class="amount">$115.00</b> </td>
											</tr>
											</tfoot>
										</table>
									</div>
								</div>

								<div id="payment" class="col-lg-6 col-sm-6 border woocommerce-checkout-payment">
									<h4 class="cart-title-highlight title-3">Your Payment</h4>

									<div class="woocommerce-checkout-payment-inner">
										<ul class="payment_methods methods list-unstyled">
											<li class="payment_method_bacs">
												<div class="form-group">
													<label class="radio-inline"> <input type="radio" value="" name="payment_method"> <span> Direct Bank Transfer </span>  </label>
												</div>

												<div class="payment_box payment_method_bacs" >
													<p> <strong> Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won&#8217;t be shipped until the funds have cleared in our account. </strong> </p>
												</div>
											</li>
											<li class="payment_method_cod">
												<div class="form-group">
													<label class="radio-inline"> <input type="radio" value="" name="payment_method"> <span> Cash on Delivery </span>  </label>
												</div>
												<div class="payment_box payment_method_cod">
													<p> <strong> Pay with cash upon delivery. </strong> </p>
												</div>
											</li>
											<li class="payment_method_paypal">
												<div class="form-group">
													<label class="radio-inline"> <input type="radio" value="" name="payment_method"> <span> PayPal </span>  </label>
												</div>
												<div class="payment_box payment_method_paypal">
													<p> <strong> Pay via PayPal; you can pay with your credit card if you don&#8217;t have a PayPal account. </strong> </p>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="wc-proceed-to-checkout text-center">
									<a class="checkout-button button alt wc-forward" href="checkout.html">
										<i class="fa fa-check-circle"></i>Proceed to Checkout
									</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</article>
		</main>
	</div>

	<div class="clear"></div>
</div>
<!-- / CONTENT + SIDEBAR -->

<?=$this->element('Go/newsletter');?>
<?=$this->element('Go/footer');?>
<?=$this->element('Go/search-modal');?>

<!-- Top -->
<div class="to-top" id="to-top"> <i class="fa fa-long-arrow-up"></i> </div>

<!-- JS Global -->
<script src="/plugins/jquery/jquery-2.1.3.js"></script>
<script src="/plugins/royalslider/jquery.royalslider.min.js"></script>
<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/plugins/bootstrap-select-1.9.3/dist/js/bootstrap-select.min.js"></script>
<script src="/plugins/owl-carousel2/owl.carousel.min.js"></script>
<script src="/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/plugins/isotope-master/dist/isotope.pkgd.min.js"></script>

<!-- Page JS -->
<script src="/js/jquery.sticky.js"></script>
<script src="/js/custom.js"></script>

</body>
</html>