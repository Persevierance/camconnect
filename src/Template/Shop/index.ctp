<?php
$this->layout = false;
$pageTitle = 'TheCamConnect | Shop';
?>
<!DOCTYPE html>
<html lang="en">
<?=$this->element('Go/head');?>
<body class="single single-product woocommerce woocommerce-page">

<!-- HEADER -->
<header id="masthead" class="clearfix" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
	<?=$this->element('Go/site-header');?>
	<?=$this->element('Go/top-nav');?>
</header>
<!-- / HEADER -->
<!-- CONTENT + SIDEBAR -->
<div class="main-wrapper clearfix">
	<?=$this->element('Go/carousel');?>
	<section class="gst-spc1 row-arrivals woocommerce ovh light-bg">
		<div class="container theme-container">
			<div class="gst-column col-lg-12 no-padding text-center">
				<div class="fancy-heading text-center">
					<h3><span class="thm-clr">New</span> Arrivals</h3>
					<h5 class="funky-font-2">Trending Cameras</h5>
				</div>
				<!-- Filter for items -->
				<div class="clearfix tabs space-15">
					<ul class="filtrable products_filter">
						<li class="active"><a href="#" data-filter=".cat-1">STILLS CAMERAS</a></li>
						<li class=""><a href="#" data-filter=".cat-2" >MOTION CAMERAS</a></li>
					</ul>
				</div>

				<!-- Portfolio items -->
				<div class="row isotope isotope-items cat-filter hvr2">
					<div class="col-md-3 col-sm-6 col-xs-12 isotope-item cat-1">
						<div class="portfolio-wrapper">
							<div class="portfolio-thumb">
								<img src="/img/products/NIKON-D4-1.png" alt="">
								<div class="portfolio-content">
									<div class="rating">
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star"></span>
										<span class="star"></span>
									</div>
									<div class="pop-up-icon">
										<a  data-toggle="modal" href="#product-preview" class="center-link"><i class="fa fa-search"></i></a>
										<a href="#" class="left-link"><i class="fa fa-heart"></i></a>
										<a class="right-link" href="#"><i class="cart-icn"> </i></a>
									</div>
									<div class="all-view">
										<a href="#" class="fancy-btn-alt fancy-btn-small">View All Product</a>
									</div>
								</div>
							</div>
							<div class="product-content">
								<h3> <a class="title-3 fsz-16" href="#"> NIKON D4 </a> </h3>
								<p class="font-3">Price: <span class="thm-clr"> £99.00 Per Day</span> </p>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 isotope-item cat-2">
						<div class="portfolio-wrapper">
							<div class="portfolio-thumb">
								<img src="/img/products/product.png" alt="">
								<div class="portfolio-content">
									<div class="rating">
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star"></span>
										<span class="star"></span>
									</div>
									<div class="pop-up-icon">
										<a  data-toggle="modal" href="#product-preview" class="center-link"><i class="fa fa-search"></i></a>
										<a href="#" class="left-link"><i class="fa fa-heart"></i></a>
										<a class="right-link" href="#"><i class="cart-icn"> </i></a>
									</div>
									<div class="all-view">
										<a href="#" class="fancy-btn-alt fancy-btn-small">View All Jeans</a>
									</div>
								</div>
							</div>
							<div class="product-content">
								<h3> <a class="title-3 fsz-16" href="#"> NIKON EOS 5D </a> </h3>
								<p class="font-3">Price: <span class="thm-clr"> £129.00 Per Week</span> </p>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 isotope-item cat-1">
						<div class="portfolio-wrapper">
							<div class="portfolio-thumb">
								<img src="/img/products/product.png" alt="">
								<div class="portfolio-content">
									<div class="rating">
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star"></span>
										<span class="star"></span>
									</div>
									<div class="pop-up-icon">
										<a  data-toggle="modal" href="#product-preview" class="center-link"><i class="fa fa-search"></i></a>
										<a href="#" class="left-link"><i class="fa fa-heart"></i></a>
										<a class="right-link" href="#"><i class="cart-icn"> </i></a>
									</div>
									<div class="all-view">
										<a href="#" class="fancy-btn-alt fancy-btn-small">View All Jeans</a>
									</div>
								</div>
							</div>
							<div class="product-content">
								<h3> <a class="title-3 fsz-16" href="#">Product Name </a> </h3>
								<p class="font-3">Price: <span class="thm-clr">£190.00 per Week per Day </span> </p>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 isotope-item cat-2">
						<div class="portfolio-wrapper">
							<div class="portfolio-thumb">
								<img src="/img/products/product.png" alt="">
								<div class="portfolio-content">
									<div class="rating">
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star"></span>
										<span class="star"></span>
									</div>
									<div class="pop-up-icon">
										<a  data-toggle="modal" href="#product-preview" class="center-link"><i class="fa fa-search"></i></a>
										<a href="#" class="left-link"><i class="fa fa-heart"></i></a>
										<a class="right-link" href="#"><i class="cart-icn"> </i></a>
									</div>
									<div class="all-view">
										<a href="#" class="fancy-btn-alt fancy-btn-small">View All Jeans</a>
									</div>
								</div>
							</div>
							<div class="product-content">
								<h3> <a class="title-3 fsz-16" href="#">Product Name </a> </h3>
								<p class="font-3">Price: <span class="thm-clr">£190.00 per Week </span> </p>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 isotope-item cat-1">
						<div class="portfolio-wrapper">
							<div class="portfolio-thumb">
								<img src="/img/products/product.png" alt="">
								<div class="portfolio-content">
									<div class="rating">
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star half"></span>
									</div>
									<div class="pop-up-icon">
										<a  data-toggle="modal" href="#product-preview" class="center-link"><i class="fa fa-search"></i></a>
										<a href="#" class="left-link"><i class="fa fa-heart"></i></a>
										<a class="right-link" href="#"><i class="cart-icn"> </i></a>
									</div>
									<div class="all-view">
										<a href="#" class="fancy-btn-alt fancy-btn-small">View All Jeans</a>
									</div>
								</div>
							</div>
							<div class="product-content">
								<h3> <a class="title-3 fsz-16" href="#">Product Name </a> </h3>
								<p class="font-3">Price: <span class="thm-clr">£90.00 per Day </span> </p>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 isotope-item cat-2">
						<div class="portfolio-wrapper">
							<div class="portfolio-thumb">
								<img src="/img/products/product.png" alt="">
								<div class="portfolio-content">
									<div class="rating">
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star"></span>
										<span class="star"></span>
									</div>
									<div class="pop-up-icon">
										<a  data-toggle="modal" href="#product-preview" class="center-link"><i class="fa fa-search"></i></a>
										<a href="#" class="left-link"><i class="fa fa-heart"></i></a>
										<a class="right-link" href="#"><i class="cart-icn"> </i></a>
									</div>
									<div class="all-view">
										<a href="#" class="fancy-btn-alt fancy-btn-small">View All Jeans</a>
									</div>
								</div>
							</div>
							<div class="product-content">
								<h3> <a class="title-3 fsz-16" href="#"> Product Name </a> </h3>
								<p class="font-3">Price: <span class="thm-clr">£190.00 per Week </span> </p>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 isotope-item cat-1">
						<div class="portfolio-wrapper">
							<div class="portfolio-thumb">
								<img src="/img/products/product.png" alt="">
								<div class="portfolio-content">
									<div class="rating">
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star"></span>
										<span class="star"></span>
									</div>
									<div class="pop-up-icon">
										<a  data-toggle="modal" href="#product-preview" class="center-link"><i class="fa fa-search"></i></a>
										<a href="#" class="left-link"><i class="fa fa-heart"></i></a>
										<a class="right-link" href="#"><i class="cart-icn"> </i></a>
									</div>
									<div class="all-view">
										<a href="#" class="fancy-btn-alt fancy-btn-small">View All Jeans</a>
									</div>
								</div>
							</div>
							<div class="product-content">
								<h3> <a class="title-3 fsz-16" href="#"> Product Name </a> </h3>
								<p class="font-3">Price: <span class="thm-clr">£90.00 per Day </span> </p>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12 isotope-item cat-2">
						<div class="portfolio-wrapper">
							<div class="portfolio-thumb">
								<img src="/img/products/product.png" alt="">
								<div class="portfolio-content">
									<div class="rating">
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star active"></span>
										<span class="star"></span>
										<span class="star"></span>
									</div>
									<div class="pop-up-icon">
										<a  data-toggle="modal" href="#product-preview" class="center-link"><i class="fa fa-search"></i></a>
										<a href="#" class="left-link"><i class="fa fa-heart"></i></a>
										<a class="right-link" href="#"><i class="cart-icn"> </i></a>
									</div>
									<div class="all-view">
										<a href="#" class="fancy-btn-alt fancy-btn-small">View All Jeans</a>
									</div>
								</div>
							</div>
							<div class="product-content">
								<h3> <a class="title-3 fsz-16" href="#"> Product Name </a> </h3>
								<p class="font-3">Price: <span class="thm-clr">£190.00 per Week </span> </p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="gst-compare dscnt-bnnr">

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 dscnt-3">
			<div class="col-lg-7 no-padding right">
				<h5 class="title-1 fsz-35">Popular Products</h5>

				<h3 class="sec-title fsz-35">
					<span class="thm-clr"> Still </span>  Cameras
				</h3>
				<p class="fsz-16 blklt-clr no-mrgn">SEE OUR RANGE OF <b class="fw900">STILL CAMERAS</b></p>
				<p> <i class="thm-clr">More advanced stills cameras and equipment are introduced almost every week</i> </p>
				<a class="smpl-btn view-all" href="#"> SHOP ALL NOW </a>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 dscnt-4">
			<div class="col-lg-8 no-padding wht-clr">
				<h5 class="title-1 fsz-35"> Latest Arrivals </h5>
				<h5 class="sec-title fsz-35"> <span class="clr1"> Motion </span>Cameras</h5>
				<p class="fsz-16 no-mrgn">CHECK OUT OUR <b class="fw900">NEW ARRIVAL</b></p>
				<p> <i> Hire from the latest motion video cameras for video shoots or just to test them prior to buying one outright</i> </p>
				<a class="smpl-btn view-all" href="#"> SHOP MOTION CAMERAS</a>
			</div>
		</div>

		<div class="descount bold-font-2"> <div class="rel-div"> <p>UP TO 75% OFF</p> </div> </div>
	</section>

	<section class="service-sec">
		<div class="container theme-container">
			<div class="service">
				<div class="row">
					<div class="col-sm-6 text-center">
						<div class="left">
							<div class="icon"><img src="/img/extra/icon-delivery.png" alt="Delivery"></div>
						</div>

						<p class="title-3 fsz-18">Fast delivery</p>
						<p class="second-heading">Did you know that we ship to anywhere in the UK.</p>
					</div>

<!--					<div class="col-sm-4">-->
<!--						<div class="left">-->
<!--							<div class="icon"><img src="/img/extra/icon-money-back.png" alt="Money back"></div>-->
<!--						</div>-->
<!--						<p class="title-3 fsz-18"></p>-->
<!--						<p class="second-heading"></p>-->
<!--					</div>-->

					<div class="col-sm-6">
						<div class="left">
							<div class="icon"><img src="/img/extra/icon-support.png" alt="Support"></div>
						</div>

						<p class="title-3 fsz-18">Awesome support</p>
						<p class="second-heading">Benefit from our 24 hour online or phoneline support.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="clear"></div>
</div>

<?=$this->element('Go/newsletter');?>
<?=$this->element('Go/footer');?>

<?=$this->element('Go/search-modal');?>
<?=$this->element('Go/login-modal');?>

<!-- Top -->
<div class="to-top" id="to-top"><i class="fa fa-long-arrow-up"></i></div>

<!-- JS Global -->
<script src="/plugins/jquery/jquery-2.1.3.js"></script>
<script src="/plugins/royalslider/jquery.royalslider.min.js"></script>
<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/plugins/bootstrap-select-1.9.3/dist/js/bootstrap-select.min.js"></script>
<script src="/plugins/owl-carousel2/owl.carousel.min.js"></script>
<script src="/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/plugins/isotope-master/dist/isotope.pkgd.min.js"></script>

<!-- Page JS -->
<script src="/js/jquery.sticky.js"></script>
<script src="/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/js/custom.js"></script>
<script>
	$('.datepicker').datepicker({
		maxViewMode: 1,
		orientation: "bottom left",
		toggleActive: true,
		autoclose: true
	});
</script>
</body>
</html>
