<?php
$this->layout = false;
$pageTitle = 'TheCamConnect | Products';
?>
<!DOCTYPE html>
<html lang="en">
<?=$this->element('Go/head');?>
<body class="single single-product woocommerce woocommerce-page">

<!-- HEADER -->
<header id="masthead" class="clearfix" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
	<div class="site-subheader site-header">
		<div class="container theme-container">
			<!-- Language & Currency Switcher -->
			<ul class="pull-left list-unstyled list-inline">
				<li class="nav-dropdown language-switcher">
					<div>EN</div>
					<ul class="nav-dropdown-inner list-unstyled list-lang">
						<li><span class="current">EN</span></li>
						<li><a title="Russian" href="#">RU</a></li>
						<li><a title="France" href="#">FR</a></li>
						<li><a title="Brazil" href="#">IT</a></li>
					</ul>
				</li>
				<li class="nav-dropdown language-switcher">
					<div><span class="fa fa-dollar"></span>USD</div>
					<ul class="nav-dropdown-inner list-unstyled list-currency">
						<li><span class="current"><span class="fa fa-dollar"></span>USD</span></li>
						<li><a title="Euro" href="#"><span class="fa fa-eur"></span>Euro</a></li>
						<li><a title="GBP" href="#"><span class="fa fa-gbp"></span>GBP</a></li>
					</ul>
				</li>
			</ul>
			
			<!-- Mini Cart -->
			<ul class="pull-right list-unstyled list-inline">
				<li class="nav-dropdown">
					<a href="#">My Account</a>
					<ul class="nav-dropdown-inner list-unstyled accnt-list">
						<li><a href="my-account.html">My Account</a></li>
						<li><a href="account-info.html"> Account Information </a></li>
						<li><a href="cng-pw.html">Change Password</a></li>
						<li><a href="address-book.html">Address Books</a></li>
						<li><a href="order-history.html">Order History</a></li>
						<li><a href="review-rating.html">Reviews and Ratings</a></li>
						<li><a href="return.html">Returns Requests</a></li>
						<li><a href="newsletter.html">Newsletter</a></li>
						<li><a href="myaccount-leftsidebar.html">Left Sidebar</a></li>
					</ul>
				</li>
				<li id="cartContent" class="cartContent">
					<a id="miniCartDropdown" href="cart.html">
						My Cart
						<span class="cart-item-num">0</span>
					</a>
					
					<div id="miniCartView" class="cartView">
						<ul id="minicartHeader" class="product_list_widget list-unstyled">
							<li>
								<div class="media clearfix">
									<div class="media-lefta">
										<a href="single-product.html">
											<img src="/img/products/theme/theme/cart-popup-1.jpg" alt="hoodie_5_front"/>
										</a>
									</div>
									<div class="media-body">
										<a href="single-product.html">Flusas Feminin</a>
										<span class="price"><span class="amount"><span class="fa fa-dollar"></span>20.00</span></span>
										<span class="quantity">Qty:  1Pcs</span>
									</div>
								</div>
								
								<div class="product-remove">
									<a href="#" class="btn-remove" title="Remove this item"><i class="fa fa-close"></i></a>
								</div>
							</li>
							<li>
								<div class="media clearfix">
									<div class="media-lefta">
										<a href="single-product.html">
											<img src="/img/products/theme/theme/cart-popup-2.jpg" alt="T_2_front"/>
										</a>
									</div>
									<div class="media-body">
										<a href="single-product.html">Autum Winter</a>
										<span class="price"><span class="amount"><span class="fa fa-dollar"></span>20.00</span></span>
										<span class="quantity">Qty:  1Pcs</span>
									</div>
								</div>
								
								<div class="product-remove">
									<a href="#" class="btn-remove" title="Remove this item"><i class="fa fa-close"></i></a>
								</div>
							</li>
							<li>
								<div class="media clearfix">
									<div class="media-lefta">
										<a href="single-product.html">
											<img src="/img/products/theme/theme/cart-popup-3.jpg" alt="cd_6_angle"/>
										</a>
									</div>
									<div class="media-body">
										<a href="single-product.html">Women's Summer</a>
										<span class="price"><span class="amount"><span class="fa fa-dollar"></span>20.00</span></span>
										<span class="quantity">Qty:  1Pcs</span>
									</div>
								</div>
								
								<div class="product-remove">
									<a href="#" class="btn-remove" title="Remove this item"><i class="fa fa-close"></i></a>
								</div>
							</li>
						</ul>
						
						<div class="cartActions">
							<span class="pull-left">Subtotal</span>
							<span class="pull-right"><span class="amount"><span class="fa fa-dollar"></span>75.00</span></span>
							<div class="clearfix"></div>
							
							<div class="minicart-buttons">
								<div class="col-lg-6">
									<a href="cart.html">Your Cart</a>
								</div>
								<div class="col-lg-6">
									<a href="checkout.html" class="minicart-checkout">Checkout</a>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</li>
				<li class="menu-item">
					<a href="checkout.html">Checkout</a>
				</li>
				<li class="menu-item">
					<a href="#login-popup" data-toggle="modal">Login</a>
				</li>
			</ul>
		</div>
	</div>
	<?=$this->element('Go/top-nav');?>
</header>
<!-- / HEADER -->

<!-- CONTENT + SIDEBAR -->
<div class="main-wrapper clearfix">
	<?=$this->element('Go/breadcrumb');?>

	<div class="clear"></div>
	<div class="theme-container container">
		<div class="main-container row">
			<main class="col-md-12 shop-full-wrap">
				<div class="row spcbt-30">
				<div class="col-lg-3 col-sm-4 sorter">
					<ul class="nav-tabs tabination view-tabs">
						<li class="active">
							<a href="#grid-view" data-toggle="tab">
								<i class="fa fa-th" aria-hidden="true"></i>
							</a>
						</li>
						<li class="">
							<a href="#list-view" data-toggle="tab">
								<i class="fa fa-th-list"></i>
							</a>
						</li>
					</ul>
					<form action="#" class="sorting-form">
						<div class="search-selectpicker selectpicker-wrapper">
							<select
									class="selectpicker input-price" data-live-search="true" data-width="100%"
									data-toggle="tooltip" title="Sort By">
								<option value="popularity">Sort by popularity</option>
								<option value="rating">Sort by average rating</option>
								<option value="date">Sort by newness</option>
								<option value="price">Sort by price: low to high</option>
								<option value="price-desc">Sort by price: high to low</option>
							</select>
						</div>
					</form>
				</div>

				<div class="col-lg-4 col-sm-4 woocommerce-result-count">  SHOW 24 ITEMS TOTAL OF 120 ITEMS </div>

				<div class="col-lg-5 col-sm-4 col-xs-12 view-wrap">
					<div class="right products-number-selector">
						<span> <a href="#"> View All </a></span>
						<span><a href="?productnumber=9" class="highlight-selector">9</a></span>
						<span><a href="?productnumber=12">12</a></span>
						<span><a href="?productnumber=24">24</a></span>
					</div>

				</div>
			</div>

				<div class="tab-content">
				<!-- Product Grid View -->
				<div id="grid-view" class="tab-pane fade active in" role="tabpanel">
					<div class="row text-center hvr2 clearfix">
						<?php foreach($products as $product){?>
							<div class="col-md-3 col-sm-4">
								<div class="portfolio-wrapper">
									<div class="portfolio-thumb">
										<img src="/img/products/<?= empty($product['images']) ? 'product.png' :  $product['images'][0]['name']?>" alt="" class="img-responsive">
										<div class="portfolio-content">
											<div class="rating">
												<span class="star active"></span>
												<span class="star active"></span>
												<span class="star active"></span>
												<span class="star"></span>
												<span class="star"></span>
											</div>
											<div class="pop-up-icon">
												<a  data-toggle="modal" href="#product-preview" class="center-link"><i class="fa fa-search"></i></a>
												<a href="#" class="left-link"><i class="fa fa-heart"></i></a>
												<a class="right-link" href="#"><i class="cart-icn"> </i></a>
											</div>
										</div>
									</div>
									<div class="product-content">
										<h3> <a class="title-3 fsz-16" href="/Shop/Product/<?=$product['id']?>"> <?=$product['name']?> </a> </h3>
										<p class="font-3">Price: <span class="thm-clr"> £ <?=$product['price'] .' <small>per</small> '. $product['price_per']?></span> </p>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
					<nav class="woocommerce-pagination">
						<ul class="page-numbers">
							<li><a class="next page-numbers" href="#"> <i class="fa fa-angle-left"></i> </a></li>
							<li><span class="page-numbers current">1</span></li>
							<li><a class="page-numbers" href="#">2</a></li>
							<li><a class="next page-numbers" href="#"> <i class="fa fa-angle-right"></i> </a></li>
						</ul>
					</nav>
				</div>
				<!-- / Product Grid View -->

				<!-- Product List View -->
				<div id="list-view" class="tab-pane fade" role="tabpanel">
					<div class="cat-list-view">
						<div class="hvr2 row">
							<div class="portfolio-wrapper">
								<div class="col-md-3 col-sm-5">
									<div class="portfolio-thumb">
										<img src="/img/products/theme/cat-1.jpg" alt="">
										<div class="portfolio-content">
											<div class="pop-up-icon">
												<a class="center-link" href="#product-preview" data-toggle="modal"><i class="fa fa-search"></i></a>
												<a class="left-link" href="#"><i class="fa fa-heart"></i></a>
												<a class="right-link" href="#"><i class="cart-icn"> </i></a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-9 col-sm-7">
									<div class="product-content">
										<a class="title-3 fsz-16" href="#"> CICLYSMO JACKET </a>
										<div class="rating">
											<span class="star active"></span>
											<span class="star active"></span>
											<span class="star active"></span>
											<span class="star"></span>
											<span class="star"></span>
										</div>
										<p class="font-3">Price: <span class="thm-clr"> $299.00 </span> </p>
										<p class="font-3"> Available:<span class="grn-clr"> In Stock </span>  </p>
										<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc condimentum eros idoni
											rutrum fermentum. Proin nec felis dui. Vestibulum ante ipsum primis in faucibus orci
											luctus et ultrices posuere cubilia Curae.</p>
										<a class="fancy-btn fancy-btn-small" href="single-product.html">Add to Cart</a>
									</div>
								</div>
							</div>
						</div>
						<div class="hvr2 row">
							<div class="portfolio-wrapper">
								<div class="col-md-3 col-sm-5">
									<div class="portfolio-thumb">
										<img src="/img/products/theme/cat-2.jpg" alt="">
										<div class="portfolio-content">
											<div class="pop-up-icon">
												<a class="center-link" href="#product-preview" data-toggle="modal"><i class="fa fa-search"></i></a>
												<a class="left-link" href="#"><i class="fa fa-heart"></i></a>
												<a class="right-link" href="#"><i class="cart-icn"> </i></a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-9 col-sm-7">
									<div class="product-content">
										<a class="title-3 fsz-16" href="#">  LYCRA BITZ MEN CLOTHING  </a>
										<div class="rating">
											<span class="star active"></span>
											<span class="star active"></span>
											<span class="star active"></span>
											<span class="star"></span>
											<span class="star"></span>
										</div>
										<p class="font-3">Price: <span class="thm-clr"> $299.00 </span> </p>
										<p class="font-3"> Available:<span class="red-clr"> Out Of Stock </span>  </p>
										<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc condimentum eros idoni
											rutrum fermentum. Proin nec felis dui. Vestibulum ante ipsum primis in faucibus orci
											luctus et ultrices posuere cubilia Curae.</p>
										<a class="fancy-btn fancy-btn-small" href="single-product.html">Add to Cart</a>
									</div>
								</div>
							</div>
						</div>
						<div class="hvr2 row">
							<div class="portfolio-wrapper">
								<div class="col-md-3 col-sm-5">
									<div class="portfolio-thumb">
										<img src="/img/products/theme/cat-3.jpg" alt="">
										<div class="portfolio-content">
											<div class="pop-up-icon">
												<a class="center-link" href="#product-preview" data-toggle="modal"><i class="fa fa-search"></i></a>
												<a class="left-link" href="#"><i class="fa fa-heart"></i></a>
												<a class="right-link" href="#"><i class="cart-icn"> </i></a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-9 col-sm-7">
									<div class="product-content">
										<a class="title-3 fsz-16" href="#"> CICLYSMO JACKET </a>
										<div class="rating">
											<span class="star active"></span>
											<span class="star active"></span>
											<span class="star active"></span>
											<span class="star active"></span>
											<span class="star half"></span>
										</div>
										<p class="font-3">Price: <span class="thm-clr"> $299.00 </span> </p>
										<p class="font-3"> Available:<span class="grn-clr"> In Stock </span>  </p>
										<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc condimentum eros idoni
											rutrum fermentum. Proin nec felis dui. Vestibulum ante ipsum primis in faucibus orci
											luctus et ultrices posuere cubilia Curae.</p>
										<a class="fancy-btn fancy-btn-small" href="single-product.html">Add to Cart</a>
									</div>
								</div>
							</div>
						</div>

						<div class="hvr2 row">
							<div class="portfolio-wrapper">
								<div class="col-md-3 col-sm-5">
									<div class="portfolio-thumb">
										<img src="/img/products/theme/cat-4.jpg" alt="">
										<div class="portfolio-content">
											<div class="pop-up-icon">
												<a class="center-link" href="#product-preview" data-toggle="modal"><i class="fa fa-search"></i></a>
												<a class="left-link" href="#"><i class="fa fa-heart"></i></a>
												<a class="right-link" href="#"><i class="cart-icn"> </i></a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-9 col-sm-7">
									<div class="product-content">
										<a class="title-3 fsz-16" href="#"> LYCRA BITZ MEN CLOTHING </a>
										<div class="rating">
											<span class="star active"></span>
											<span class="star active"></span>
											<span class="star active"></span>
											<span class="star"></span>
											<span class="star"></span>
										</div>
										<p class="font-3">Price: <span class="thm-clr"> $299.00 </span> </p>
										<p class="font-3"> Available:<span class="grn-clr"> In Stock </span>  </p>
										<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc condimentum eros idoni
											rutrum fermentum. Proin nec felis dui. Vestibulum ante ipsum primis in faucibus orci
											luctus et ultrices posuere cubilia Curae.</p>
										<a class="fancy-btn fancy-btn-small" href="single-product.html">Add to Cart</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<nav class="woocommerce-pagination">
						<ul class="page-numbers">
							<li><a class="next page-numbers" href="#"> <i class="fa fa-angle-left"></i> </a></li>
							<li><span class="page-numbers current">1</span></li>
							<li><a class="page-numbers" href="#">2</a></li>
							<li><a class="next page-numbers" href="#"> <i class="fa fa-angle-right"></i> </a></li>
						</ul>
					</nav>
				</div>
				<!-- / Product List View -->
			</div>
			</main>
		</div>
	</div>

	<div class="clear"></div>
</div>

<?=$this->element('Go/newsletter');?>
<?=$this->element('Go/footer');?>

<?=$this->element('Go/search-modal');?>
<?=$this->element('Go/login-modal');?>

<!-- Top -->
<div class="to-top" id="to-top"><i class="fa fa-long-arrow-up"></i></div>

<!-- JS Global -->
<script src="/plugins/jquery/jquery-2.1.3.js"></script>
<script src="/plugins/royalslider/jquery.royalslider.min.js"></script>
<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/plugins/bootstrap-select-1.9.3/dist/js/bootstrap-select.min.js"></script>
<script src="/plugins/owl-carousel2/owl.carousel.min.js"></script>
<script src="/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/plugins/isotope-master/dist/isotope.pkgd.min.js"></script>

<!-- Page JS -->
<script src="/js/jquery.sticky.js"></script>
<script src="/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/js/custom.js"></script>
<script>
	$('.datepicker').datepicker({
		maxViewMode: 1,
		orientation: "bottom left",
		toggleActive: true,
		autoclose: true
	});
</script>
</body>
</html>
