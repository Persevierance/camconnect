<?php
$this->layout = false;
$pageTitle = 'TheCamConnect | ' . $product['name'];
?>
<!DOCTYPE html>
<html lang="en">
<?=$this->element('Go/head');?>
<body class="single single-product woocommerce woocommerce-page">

<!-- HEADER -->
<header id="masthead" class="clearfix" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
	<?=$this->element('Go/site-header')?>
	<?=$this->element('Go/top-nav');?>
</header>
<!-- / HEADER -->

<!-- CONTENT + SIDEBAR -->
<div class="main-wrapper clearfix">
	<?=$this->element('Go/breadcrumb');?>

	<div class="theme-container container">
		<main id="main-content" class="main-content">
			<div itemscope itemtype="http://schema.org/Product"
			     class="product has-post-thumbnail product-type-variable">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div id="gallery-2" class="royalSlider bg-muted rsUni">
							<?php if(!empty($productImages)){ foreach($productImages as $image){?>
								<a class="rsImg img-responsive img-thumbnail" data-rsbigimg="/img/products/<?=$image['name']?>" href="/img/products/<?=$image['name']?>" data-rsw="500" data-rsh="500"> <img class="rsTmb img-responsive" src="/img/products/<?=$image['name']?>" alt=""></a>
							<?php } } else {?>
								<a class="rsImg img-responsive img-thumbnail" data-rsbigimg="/img/products/product.png" href="/img/products/product.png" data-rsw="500" data-rsh="500"> <img class="rsTmb img-responsive" src="/img/products/product.png" alt=""></a>
							<?php } ?>
						</div>
					</div>
<!--					<div class="spc-15 hidden-lg clear"></div>-->
					<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
						<div class="summary entry-summary">
							<div class="woocommerce-product-rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
								<div class="rating">
									<span class="star active"></span>
									<span class="star active"></span>
									<span class="star active"></span>
									<span class="star active"></span>
									<span class="star half"></span>
								</div>
								
								<div class="posted_in">
									<h3 class="funky-font-2 fsz-20">Category <i class="fa fa-chevron-circle-right"></i> <?=$product['manufacturer_id']?></h3>
								</div>
							</div>
							
							<div class="product_title_wrapper">
								<div itemprop="name" class="product_title entry-title">
									<span class="thm-clr"><?=$product['name']?></span>
									<p class="font-3 fsz-18 no-mrgn price text-capitalize">
										<b class="amount blk-clr">£ <?=$product['price']?></b> per <?=$product['pricing']?>
									</p>
								</div>
							</div>
							
							<div itemprop="description" class="fsz-15">
								<p class="text-justify"><?= substr($product['description'],0,300 )?>...</p>
							</div>
							
							<ul class="stock-detail list-items fsz-12">
								<li><strong> DEPOSIT : <span class="blk-clr"> £<?=$product['deposit']?></span> </strong></li>
								<li><strong> <?=$product['quantity']?> Available :
										<?php if($product['quantity'] > 0){
											echo '<span class="font-20 funky-font-2 thm-clr">';
											echo'Ready to order Now';
										}else{
											echo '<span class="font-20 funky-font-2 thm-clr">';
											echo'Check Next Availability';
										} ?>
								</span></strong> </li>
							</ul>
							
							<form class="variations_form cart" method="post">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group selectpicker-wrapper">
											<label class="fsz-15 title-3"><b> How long do you need it for? </b> </label>
											<div class="search-selectpicker selectpicker-wrapper">
												<select class="selectpicker input-price" data-live-search="true" data-width="100%" data-toggle="tooltip" title="No of Days">
													<option value="1">1 Day</option>
													<option value="2">2 Day</option>
													<option value="3">3 Day</option>
													<option value="4">4 Day</option>
													<option value="5">5 Day</option>
													<option value="6">6 Day</option>
													<option value="7">7 Day</option>
													<option value="8">8 Day</option>
													<option value="9">9 Day</option>
													<option value="10">10 Day</option>
													<option value="11">11 Day</option>
													<option value="12">12 Day</option>
													<option value="13">13 Day</option>
													<option value="14">14 Day</option>
													<option value="15">15 Day</option>
													<option value="16">16 Day</option>
													<option value="17">17 Day</option>
													<option value="18">18 Day</option>
													<option value="19">19 Day</option>
													<option value="20">20 Day</option>
													<option value="21">21 Day</option>
													<option value="22">22 Day</option>
													<option value="23">23 Day</option>
													<option value="24">24 Day</option>
													<option value="25">25 Day</option>
													<option value="26">26 Day</option>
													<option value="27">27 Day</option>
													<option value="28">28 Day</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group selectpicker-wrapper">
											<label class="fsz-15 title-3"> <b> When do you need it? </b> </label>
											<div class=" input-group date">
												<input type="text" class="form-control datepicker" value="<?=date('m/d/Y')?>" id="datepicker"/>
												<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<button type="submit"
											        class="single_add_to_cart_button button alt fancy-button left"><i class="fa fa-check"></i> Check Availability
											</button>
										</div>
									</div>
								</div>
							</form>
						</div><!-- .summary -->
					</div>
				</div>
				<div class="clearfix"></div>
				
				<div class="woocommerce-tabs wc-tabs-wrapper row">
					<ul class="tabs wc-tabs">
						<li class="description_tab">
							<a href="#tab-description">Description</a>
						</li>
						<li class="additional_information_tab">
							<a href="#tab-additional_information">Additional Information</a>
						</li>
						<li class="reviews_tab">
							<a href="#tab-reviews">Reviews (3)</a>
						</li>
					</ul>
					
					<div class="entry-content wc-tab col-lg-4 col-sm-6 col-xs-12" id="tab-description">
						<h2 class="title-3">Description</h2>
						<hr class="heading-seperator"/>
						<div class="scroll-div">
							<div class="nano-content text-justify">
								<?=$product['description']?>
							</div>
						</div>
					</div>
					
					<div class="entry-content wc-tab col-lg-4 col-sm-6 col-xs-12" id="tab-reviews">
						<h2 class="title-3">Product Review</h2>
						<hr class="heading-seperator"/>
						<div class="scroll-div">
							<div class="nano-content">
								<div id="reviews">
									<div id="comments">
										<ol class="commentlist">
											<li itemprop="review" itemscope itemtype="http://schema.org/Review"
											    class="comment even thread-even depth-1">
												<div class="comment_container diblock">
													<img alt="" src="/img/extra/review-1.jpg" itemprop="image"
													     class="avatar" height="60" width="60"/>
													<div class="comment-text">
														<strong class="name">JOHN LENNON</strong>
														<div class="rating">
															<span class="star active"></span>
															<span class="star active"></span>
															<span class="star active"></span>
															<span class="star active"></span>
															<span class="star half"></span>
														</div>
														<p class="meta">
															<time itemprop="datePublished"
															      datetime="2013-06-07T13:03:29+00:00"> 2 June, 2016
															</time>
															:
														</p>
														<div itemprop="description" class="description">
															<p>The Best Camera l have used so far.</p>
														</div>
													</div>
												</div>
											</li><!-- #comment-## -->
											
											<li itemprop="review" itemscope itemtype="http://schema.org/Review"
											    class="comment even thread-even depth-1">
												<div class="comment_container diblock">
													<img alt="" src="/img/extra/review-1.jpg" itemprop="image"
													     class="avatar" height="60" width="60"/>
													<div class="comment-text">
														<strong class="name">JOHN LENNON</strong>
														<div class="rating">
															<span class="star active"></span>
															<span class="star active"></span>
															<span class="star active"></span>
															<span class="star active"></span>
															<span class="star half"></span>
														</div>
														<p class="meta">
															<time itemprop="datePublished"
															      datetime="2013-06-07T13:03:29+00:00"> 2 June, 2016
															</time>
															:
														</p>
														<div itemprop="description" class="description">
															<p>The D300 reacts with lightning speed, powering up in a
																mere 0.13 seconds and shooting with an imperceptible
																45-millisecond shutter release lag time. </p>
														</div>
													</div>
												</div>
											</li><!-- #comment-## -->
											
											<li itemprop="review" itemscope itemtype="http://schema.org/Review"
											    class="comment even thread-even depth-1">
												<div class="comment_container diblock">
													<img alt="" src="/img/extra/review-1.jpg" itemprop="image"
													     class="avatar" height="60" width="60"/>
													<div class="comment-text">
														<strong class="name">JOHN LENNON</strong>
														<div class="rating">
															<span class="star active"></span>
															<span class="star active"></span>
															<span class="star active"></span>
															<span class="star active"></span>
															<span class="star half"></span>
														</div>
														<p class="meta">
															<time itemprop="datePublished"
															      datetime="2013-06-07T13:03:29+00:00"> 2 June, 2016
															</time>
															:
														</p>
														<div itemprop="description" class="description">
															<p>Easy to user a great start for amuter photographers</p>
														</div>
													</div>
												</div>
											</li><!-- #comment-## -->
										</ol>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="entry-content wc-tab col-lg-4 col-sm-6 col-xs-12" id="tab-additional_information">
						<h2 class="title-3">Additional Information</h2>
						<hr class="heading-seperator"/>
						<div class="scroll-div">
							<div class="nano-content">
								<h2 class="title-3 fsz-14 no-mrgn spcbt-30"> ASIN: <span
											class="thm-clr"> B00IL3TMFW </span></h2>
								<p>Product Dimensions: 11 x 5.5 x 8.7 inches; 1.8 pounds.</p>
								<p>Shipping Weight: 1.8 pounds <br>
									(View shipping rates and policies)</p>
								<p>Item model number: NB-B00IL3TMFW</p>
								<p>Average Customer Review: 29 customer reviews</p>
								<p>Would you like to give feedback on images?</p>
								<p>Item model number: NB-B00IL3TMFW</p>
								<p>Average Customer Review: 29 customer reviews</p>
								<p>Would you like to give feedback on images?</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>
	<div class="clear"></div>
</div>

<?=$this->element('Go/newsletter');?>
<?=$this->element('Go/footer');?>

<?=$this->element('Go/search-modal');?>
<?=$this->element('Go/login-modal');?>

<!-- Top -->
<div class="to-top" id="to-top"><i class="fa fa-long-arrow-up"></i></div>

<!-- JS Global -->
<script src="/plugins/jquery/jquery-2.1.3.js"></script>
<script src="/plugins/royalslider/jquery.royalslider.min.js"></script>
<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/plugins/bootstrap-select-1.9.3/dist/js/bootstrap-select.min.js"></script>
<script src="/plugins/owl-carousel2/owl.carousel.min.js"></script>
<script src="/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/plugins/isotope-master/dist/isotope.pkgd.min.js"></script>

<!-- Page JS -->
<script src="/js/jquery.sticky.js"></script>
<script src="/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/js/custom.js"></script>
<script>
	$('.datepicker').datepicker({
		maxViewMode: 1,
		orientation: "bottom left",
		toggleActive: true,
		autoclose: true
	});
</script>
</body>
</html>
