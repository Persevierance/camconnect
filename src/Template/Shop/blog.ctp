<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;
$htmlTitle = 'Home';
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>The Cam Connect</title>

	<!-- Favicon -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/ico/apple-touch-icon-144-precomposed.png">
	<link rel="shortcut icon" href="/ico/favicon.ico">

	<!-- CSS Global -->
	<link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="/plugins/bootstrap-select-1.9.3/dist/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="/plugins/owl-carousel2/assets/owl.carousel.css" rel="stylesheet" type="text/css">
	<link href="/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.min.css" rel="stylesheet" type="text/css">
	<link href="/plugins/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
	<link href="/plugins/royalslider/royalslider.css" rel="stylesheet">
	<link href="/plugins/subscribe-better-master/subscribe-better.css" rel="stylesheet" type="text/css">

	<!-- Icons Font CSS -->
	<link href="/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!-- Theme CSS -->
	<link href="/css/style.css" rel="stylesheet" type="text/css">
	<link href="/css/header.css" rel="stylesheet" type="text/css">

	<!--[if lt IE 9]>
	<script src="/plugins/iesupport/html5shiv.js"></script>
	<script src="/plugins/iesupport/respond.js"></script>
	<![endif]-->

</head>
<body class="blog">

<div class="main-wrapper clearfix">
	<!-- CONTENT + SIDEBAR -->
	<section class="coming-soon-bg"  style="height: 100vh">
		<div class="theme-container container">
			<!-- Coming Soon Starts -->
			<div class="coming-wrap text-center">
				<div class="row">
					<div class="col-sm-2 col-sm-offset-5 col-xs-6 col-xs-offset-3">
						<a class="thm-logo fsz-35" href="/">
							<?= $this->Html->image('logo/logo.png',['class'=>'img-responsive']);?>
						</a>
					</div>
				</div>
				<h2 class="title-1 size-35 fsz-35 spc-15"> coming soon ! </h2>
<!--				<p class="size-16 gray-color"> <i>Please check back again within Some Days as We're Pretty Close</i> </p>-->

<!--				<div class="gst-empty-space clearfix"></div>-->
				<div id="countdown-timer1" class="gst-countdown"></div>
				<div class="gst-empty-space clearfix"></div>
				<div class="text-center well well-lg">
					<!-- Subscribe -->
<!--					<h2 class="sec-title title-1 fsz-50">Subscribe </h2>-->
					<h3 class="fsz-15 bold-font-4"><span class="thm-clr title-1"> We will let you know when we are ready.</span></h3>
					<?= $this->Form->create('',['class'=>'text-center form-inline','url'=>['controller'=>'Subscribe','action'=>'index'],'method'=>'post']);?>
						<div class="form-group"><input name="name" type="text" required="required" placeholder="Full Name" class="text-center form-control"></div>
						<div class="form-group"><input name="email" type="email" required="required" placeholder="Email address" class="text-center form-control"></div>
						<div class="form-group">
							<button class="alt fancy-button" type="submit">Subscribe  <span class="fa fa-paper-plane"></span> </button>
						</div>
					<?=$this->Form->end();?>
					<div class="gst-empty-space clearfix"></div>
					<div class="text-center"><?= $this->Flash->render();?></div>
					<!-- / Subscribe -->
				</div>
				<div class="comingsoon-media">
					<h2 class="title-1">follow us on social</h2>
					<ul class="social-icon list-items">
						<li> <a class="fa fa-twitter blklt-clr fsz-20" href="#"></a> </li>
						<li> <a class="fa fa-facebook blklt-clr fsz-20" href="#"></a> </li>
						<li> <a class="fa fa-pinterest blklt-clr fsz-20" href="#"></a> </li>
						<li> <a class="fa fa-instagram blklt-clr fsz-20" href="#"></a> </li>
						<li> <a class="fa fa-dribbble blklt-clr fsz-20" href="#"></a> </li>
					</ul>
				</div>
				<div class="gst-empty-space clearfix"></div>
				<div class="gst-empty-space clearfix"></div>
			</div>
			<!-- / Coming Soon Ends -->
		</div>
	</section>
	<!-- / CONTENT + SIDEBAR -->
</div>



<!-- JS Global -->
<script src="/plugins/jquery/jquery-2.1.3.js"></script>
<script src="/plugins/royalslider/jquery.royalslider.min.js"></script>
<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/plugins/bootstrap-select-1.9.3/dist/js/bootstrap-select.min.js"></script>
<script src="/plugins/owl-carousel2/owl.carousel.min.js"></script>
<script src="/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/plugins/isotope-master/dist/isotope.pkgd.min.js"></script>
<!--<script src="/plugins/subscribe-better-master/jquery.subscribe-better.min.js"></script>-->

<!-- Page JS -->
<script src="/js/jquery.sticky.js"></script>
<script src="/js/countdown.js"></script>
<script src="/js/custom.js"></script>
</body>
</html>

