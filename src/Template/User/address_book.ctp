<?php
$this->layout = false;
$pageTitle = 'TheCamConnect | ' . $user['firstname'] . ' Address Book';
?>

<!DOCTYPE html>
<html lang="en">
<?=$this->element('Go/head');?>
<body class="blog">
<!-- HEADER -->
<header id="masthead" class="clearfix" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
	<?=$this->element('Go/site-header')?>
	<?=$this->element('Go/top-nav');?>
</header>
<!-- / HEADER -->

<!-- CONTENT + SIDEBAR -->
<div class="main-wrapper clearfix">
	<div class="site-pagetitle jumbotron">
		<div class="container  theme-container text-center">
			<h3>My Account</h3>
			<!-- Breadcrumbs -->
			<div class="breadcrumbs">
				<div class="breadcrumbs text-center">
					<i class="fa fa-home"></i>
					<span><a href="index.html">Home</a></span>
					<i class="fa fa-arrow-circle-right"></i>
					<span class="current"> Account Information </span>
				</div>
			</div>
		</div>
	</div>

	<div class="theme-container container">
		<div class="gst-spc3 row">
			<main class="col-md-9 col-sm-8 blog-wrap">
				<article class="" itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost">
					<div class="account-details-wrap">
						<div class="heading-2">
							<h3 class="title-3 fsz-18">Change Your Personal Details</h3>
						</div>

						<div class="account-box">
							<form action="#" class="form-delivery">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group"><input class="form-control" type="text" placeholder="First Name"></div>
									</div>
									<div class="col-md-6">
										<div class="form-group"><input class="form-control" type="text" placeholder="Last Name"></div>
									</div>
									<div class="col-md-6">
										<div class="form-group"><input class="form-control" type="text" placeholder="Address 1"></div>
									</div>
									<div class="col-md-6">
										<div class="form-group"><input class="form-control" type="text" placeholder="Address 2"></div>
									</div>
									<div class="col-md-6">
										<div class="form-group selectpicker-wrapper">
											<select class="selectpicker input-price" data-live-search="true" data-width="100%" data-toggle="tooltip" title="Country">
												<option>Country</option>
												<option>Country</option>
												<option>Country</option>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group selectpicker-wrapper">
											<select class="selectpicker input-price" data-live-search="true" data-width="100%" data-toggle="tooltip" title="City">
												<option>City</option>
												<option>City</option>
												<option>City</option>
											</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group"><input class="form-control" type="text" placeholder="Postcode/ZIP"></div>
									</div>
									<div class="col-md-4">
										<div class="form-group"><input class="form-control" type="text" placeholder="Email"></div>
									</div>
									<div class="col-md-4">
										<div class="form-group"><input class="form-control" type="text" placeholder="Phone Number"></div>
									</div>
									<div class="col-md-12">
										<div class="form-group"><textarea class="form-control" placeholder="Addıtıonal Informatıon" name="name" cols="30" rows="10"></textarea></div>
									</div>
									<div class="col-md-12 col-sm-12">
										<button class="alt fancy-button" type="submit">Update</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</article>
			</main>
			<aside class="col-md-3 col-sm-4">
				<div class="main-sidebar" >
					<div id="search-2" class="widget sidebar-widget widget_search clearfix">
						<form method="get" id="searchform" class="form-search" action="http://localhost/goshopwp">
							<input class="form-control search-query" type="text" placeholder="Type Keyword" name="s" id="s" />
							<button class="btn btn-default search-button" type="submit" name="submit"><i class="fa fa-search"></i></button>
						</form>
					</div>
					<div class="widget sidebar-widget widget_categories clearfix">
						<h6 class="widget-title">My Account</h6>
						<ul>
							<li  class="accout-item"><a href="account-info.html"> Account Information </a></li>
							<li  class="accout-item active"><a href="my-account.html">My Account</a></li>
							<li  class="accout-item"><a href="cng-pw.html">Change Password</a></li>
							<li  class="accout-item"><a href="address-book.html">Address Books</a></li>
							<li  class="accout-item"><a href="order-history.html">Order History</a></li>
							<li  class="accout-item"><a href="review-rating.html">Reviews and Ratings</a></li>
							<li  class="accout-item"><a href="return.html">Returns Requests</a></li>
							<li  class="accout-item"><a href="newsletter.html">Newsletter</a></li>
							<li  class="accout-item"><a href="myaccount-leftsidebar.html">Left Sidebar</a></li>
						</ul>
					</div>
				</div>
			</aside>
		</div>
	</div>

	<div class="clear"></div>
</div>
<!-- / CONTENT + SIDEBAR -->

<?=$this->element('Go/newsletter');?>
<?=$this->element('Go/footer');?>

<?=$this->element('Go/search-modal');?>

<!-- Top -->
<div class="to-top" id="to-top"> <i class="fa fa-long-arrow-up"></i> </div>

<!-- JS Global -->
<script src="/plugins/jquery/jquery-2.1.3.js"></script>
<script src="/plugins/royalslider/jquery.royalslider.min.js"></script>
<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/plugins/bootstrap-select-1.9.3/dist/js/bootstrap-select.min.js"></script>
<script src="/plugins/owl-carousel2/owl.carousel.min.js"></script>
<script src="/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/plugins/isotope-master/dist/isotope.pkgd.min.js"></script>

<!-- Page JS -->
<script src="/js/jquery.sticky.js"></script>
<script src="/js/custom.js"></script>
</body>
</html>