<?php
	$this->layout = false;
	$pageTitle = 'TheCamConnect | ' . $user['firstname'] . ' Account';
?>

<!DOCTYPE html>
<html lang="en">
	<?=$this->element('Go/head');?>
    <body class="blog">
        <!-- HEADER -->
        <header id="masthead" class="clearfix" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
	        <?=$this->element('Go/site-header')?>
	        <?=$this->element('Go/top-nav');?>
        </header>
        <!-- / HEADER -->

        <!-- CONTENT + SIDEBAR -->
        <div class="main-wrapper clearfix">
	        <?=$this->element('Go/breadcrumb');?>

            <div class="theme-container container">
                <div class="gst-spc3 row">
                    <aside class="col-md-3 col-sm-4"><?= $this->element('Go/user_account_sidebar');?></aside>

                    <main class="col-md-9 col-sm-8 blog-wrap">
                        <article class="" itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost">
                            <div class="account-details-wrap">
                                <div class="heading-2"> <h3 class="title-3 fsz-18">My Account</h3> </div>                                
                                <div class="account-box">
                                    <ul>
                                        <li>                                                
                                            <a href="account-info.html">Edit your account information</a>
                                        </li>
                                        <li>                                               
                                            <a href="cng-pw.html">Change your password</a>
                                        </li>
                                        <li>                                              
                                            <a href="address-book.html">Modify your address book entries</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="heading-2"> <h3 class="title-3 fsz-18">order and review</h3> </div>
                                <div class="account-box">
                                    <ul>
                                        <li>
                                            <a href="order-history.html">View your order history</a>
                                        </li>
                                        <li>
                                            <a href="review-rating.html">Your reviews and ratings</a>
                                        </li>
                                        <li>
                                            <a href="return.html">View your retun requests</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="heading-2"> <h3 class="title-3 fsz-18">Newsletter</h3> </div>
                                <div class="account-box">
                                    <ul>
                                        <li>
                                            <a href="newsletter.html">Subscribe / unsubscribe to newsletter</a>
                                        </li>                                           
                                    </ul>
                                </div>
                            </div>
                        </article>
                    </main>  

                </div>
            </div>

            <div class="clear"></div>
        </div>
<!-- / CONTENT + SIDEBAR -->

        <?=$this->element('Go/newsletter');?>
        <?=$this->element('Go/footer');?>

        <?=$this->element('Go/search-modal');?>
        <?=$this->element('Go/login-modal');?>

        <!-- Top -->
        <div class="to-top" id="to-top"> <i class="fa fa-long-arrow-up"></i> </div>

        <!-- JS Global -->
        <script src="/plugins/jquery/jquery-2.1.3.js"></script>
        <script src="/plugins/royalslider/jquery.royalslider.min.js"></script>
        <script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="/plugins/bootstrap-select-1.9.3/dist/js/bootstrap-select.min.js"></script>
        <script src="/plugins/owl-carousel2/owl.carousel.min.js"></script>
        <script src="/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="/plugins/isotope-master/dist/isotope.pkgd.min.js"></script>

        <!-- Page JS -->      
        <script src="/js/jquery.sticky.js"></script>
        <script src="/js/custom.js"></script>
    </body>
</html>