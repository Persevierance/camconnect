<?php
	$this->layout = false;
	$pageTitle = 'TheCamConnect | ' . $user['firstname'] . ' Account';
?>
<!DOCTYPE html>
<html lang="en">
	<?=$this->element('Go/head');?>
	<body class="blog">
        <!-- HEADER -->
        <header id="masthead" class="clearfix" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
	        <?=$this->element('Go/site-header')?>
	        <?=$this->element('Go/top-nav');?>
        </header>
        <!-- / HEADER -->

        <!-- CONTENT + SIDEBAR -->
        <div class="main-wrapper clearfix">
            <div class="site-pagetitle jumbotron">
                <div class="container  theme-container text-center">
                    <h3>My Account</h3>
                    <!-- Breadcrumbs -->
                    <div class="breadcrumbs">
                        <div class="breadcrumbs text-center">
                            <i class="fa fa-home"></i>
                            <span><a href="index.html">Home</a></span>
                            <i class="fa fa-arrow-circle-right"></i>
                            <span class="current"> Account Information </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="theme-container container">
                <div class="gst-spc3 row">
                    <main class="col-md-9 col-sm-8 blog-wrap">
                        <article class="" itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost">
                            <div class="account-details-wrap">
                                <div class="heading-2">                                
                                    <h3 class="title-3 fsz-18">My Account Information</h3>                            
                                </div>
                                <div class="account-box">
                                    <?=$this->Form->create('account_information',['class'=>'form-delivery']);?>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group"><?=$this->Form->input('firstname',['type'=>'text','label'=>false ,'value'=>$user['firstname'],'class'=>'form-control', 'placeholder'=>'First Name', 'required']);?></div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
	                                            <div class="form-group"><?=$this->Form->input('lastname',['type'=>'text','label'=>false ,'value'=>$user['lastname'],'class'=>'form-control', 'placeholder'=>'First Name', 'required']);?></div>                                            </div>
                                            <div class="col-md-6 col-sm-6">
	                                            <div class="form-group"><?=$this->Form->input('username',['type'=>'text','label'=>false ,'value'=>$user['username'],'class'=>'form-control', 'placeholder'=>'First Name', 'required']);?></div>                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group"><input type="text" class="form-control" placeholder="Phone Number" required></div>
                                            </div>  
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group"><input type="text" class="form-control" placeholder="Fax"></div>
                                            </div> 
                                            <div class="col-md-12 col-sm-12">
                                                <button class="alt fancy-button" type="submit">Update</button>                                           
                                            </div>
                                        </div>
                                    <?=$this->Form->end();?>
                                </div>
                            </div>
                        </article>
                    </main>    

                    <aside class="col-md-3 col-sm-4"><?= $this->element('Go/user_account_sidebar');?></aside>
                </div>
            </div>

            <div class="clear"></div>
        </div>
<!-- / CONTENT + SIDEBAR -->

        <?=$this->element('Go/newsletter');?>
        <?=$this->element('Go/footer');?>

        <?=$this->element('Go/search-modal');?>
        <?=$this->element('Go/login-modal');?>

        <!-- Top -->
        <div class="to-top" id="to-top"> <i class="fa fa-long-arrow-up"></i> </div>

        <!-- JS Global -->
        <script src="/plugins/jquery/jquery-2.1.3.js"></script>
        <script src="/plugins/royalslider/jquery.royalslider.min.js"></script>
        <script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="/plugins/bootstrap-select-1.9.3/dist/js/bootstrap-select.min.js"></script>
        <script src="/plugins/owl-carousel2/owl.carousel.min.js"></script>
        <script src="/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="/plugins/isotope-master/dist/isotope.pkgd.min.js"></script>

        <!-- Page JS -->      
        <script src="/js/jquery.sticky.js"></script>
        <script src="/js/custom.js"></script>
    </body>
</html>