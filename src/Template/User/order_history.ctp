<?php
	$this->layout = false;
?>
<!DOCTYPE html>
<html lang="en">
	<?=$this->element('Go/head');?>
    <body class="blog">
        <!-- HEADER -->
        <header id="masthead" class="clearfix" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
	        <?=$this->element('Go/site-header')?>
	        <?=$this->element('Go/top-nav');?>
        </header>
        <!-- / HEADER -->

        <!-- CONTENT + SIDEBAR -->
        <div class="main-wrapper clearfix">
            <div class="site-pagetitle jumbotron">
                <div class="container  theme-container text-center">
                    <h3>My Account</h3>
                    <!-- Breadcrumbs -->
                    <div class="breadcrumbs">
                        <div class="breadcrumbs text-center">
                            <i class="fa fa-home"></i>
                            <span><a href="/">Home</a></span>
                            <i class="fa fa-arrow-circle-right"></i>
                            <span class="current"> Order History </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="theme-container container">
                <div class="spc-60 row">
                    <main class="col-sm-12">
                        <article class="woocommerce-cart">
                            <div class="account-details-wrap">
                                <div class="heading-2">                                
                                    <h3 class="title-3 fsz-18">Your Order History</h3>                            
                                </div>
                                <form action="checkout.html" method="post">
                                    <table class="shop_table product-table">
                                        <thead>
                                            <tr>
                                                <th class="product-thumbnail"> img </th>
                                                <th class="product-name">Product</th>
                                                <th class="product-price">Price</th>
                                                <th class="product-quantity">Quantity</th>
                                                <th class="product-ordid">Order ID </th>
                                                <th class="product-dil">Delivered on </th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="cart_item">
                                                <td class="product-thumbnail">
                                                    <a href="single-product.html">
                                                        <img  src="assets/img/products/cart-1.jpg" alt="poster_2_up" />
                                                    </a>
                                                </td>
                                                <td class="product-name">                                                   
                                                    <div class="cart-product-title">
                                                        <a href="single-product.html">NIKON<span class="thm-clr">D330</span> </a>
                                                    </div>
                                                </td>

                                                <td class="">                                                    
                                                    <p class="font-3 fsz-18 no-mrgn"> <b class="amount blk-clr">£175.00</b> </p>                                                    
                                                </td>

                                                <td class="">
                                                    <p class="font-3 fsz-16 blk-clr no-mrgn">  2Pcs </p>
                                                </td>
                                                <td class="order-id">
                                                    <p class="font-3 fsz-16 blk-clr no-mrgn"> OD31207 </p>
                                                </td>
                                                <td class="diliver-date">
                                                    <p class="font-3 fsz-16 blk-clr no-mrgn"> <small> 12th Dec'15 - 12th Dec'15  </small></p>
                                                </td>
                                                <td class="order-status">
                                                    <button class="alt fancy-button" type="submit">Return Order</button>
                                                    <button class="alt fancy-button-blk" type="submit">Re Order</button>                                                  
                                                </td>
                                            </tr>     
                                            <tr class="cart_item">
                                                <td class="product-thumbnail">
                                                    <a href="single-product.html">
                                                        <img  src="assets/img/products/cart-2.jpg" alt="poster_2_up" />
                                                    </a>
                                                </td>
                                                <td class="product-name">                                                   
                                                    <div class="cart-product-title">
                                                        <a href="single-product.html">NIKON<span class="thm-clr">D330</span> </a>
                                                    </div>
                                                </td>

                                                <td class="">                                                    
                                                    <p class="font-3 fsz-18 no-mrgn"> <b class="amount blk-clr">£125.00</b> </p>                                                    
                                                </td>

                                                <td class="">
                                                    <p class="font-3 fsz-16 blk-clr no-mrgn">  1Pcs </p>
                                                </td>
                                                <td class="order-id">
                                                    <p class="font-3 fsz-16 blk-clr no-mrgn"> OD31207 </p>
                                                </td>
                                                <td class="diliver-date">
                                                    <p class="font-3 fsz-16 blk-clr no-mrgn"> <small> 12th Dec'15 - 12th Dec'15  </small></p>
                                                </td>
                                                <td class="order-status">
                                                    <button class="alt fancy-button" type="submit">Return Order</button>
                                                    <button class="alt fancy-button-blk" type="submit">Re Order</button>                                                  
                                                </td>
                                            </tr>   
                                            <tr class="cart_item">
                                                <td class="product-thumbnail">
                                                    <a href="single-product.html">
                                                        <img  src="assets/img/products/cart-3.jpg" alt="poster_2_up" />
                                                    </a>
                                                </td>
                                                <td class="product-name">                                                   
                                                    <div class="cart-product-title">
                                                        <a href="single-product.html">NIKON<span class="thm-clr">D330</span> </a>
                                                    </div>
                                                </td>

                                                <td class=""><p class="font-3 fsz-18 no-mrgn"> <b class="amount blk-clr">£175.00</b> </p></td>

                                                <td class=""><p class="font-3 fsz-16 blk-clr no-mrgn">  1Pcs </p></td>
                                                <td class="order-id"><p class="font-3 fsz-16 blk-clr no-mrgn"> OD31207 </p></td>
                                                <td class="diliver-date"><p class="font-3 fsz-16 blk-clr no-mrgn "><small> 12th Dec'15 - 12th Dec'15  </small></p></td>
                                                <td class="order-status">
                                                    <button class="alt fancy-button" type="submit">Return Order</button>
                                                    <button class="alt fancy-button-blk" type="submit">Re Order</button>                                                  
                                                </td>
                                            </tr>   
                                        </tbody>
                                    </table>
                                </form>

                                <div class="spc-30 continue-shopping">                                    
                                    <div class="shp-btn">
                                        <a href="/Shop/myAccount" class="fancy-btn fancy-btn-small"> Back To Account </a>
                                    </div>                               
                                </div>
                            </div>
                        
                        </article>
                    </main>   
                </div>
            </div>

            <div class="clear"></div>
        </div>

        <?=$this->element('Go/newsletter');?>
        <?=$this->element('Go/footer');?>

        <?=$this->element('Go/search-modal');?>
        <?=$this->element('Go/login-modal');?>

        <!-- Top -->
        <div class="to-top" id="to-top"> <i class="fa fa-long-arrow-up"></i> </div>

        <!-- JS Global -->
        <script src="/plugins/jquery/jquery-2.1.3.js"></script>
        <script src="/plugins/royalslider/jquery.royalslider.min.js"></script>
        <script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="/plugins/bootstrap-select-1.9.3/dist/js/bootstrap-select.min.js"></script>
        <script src="/plugins/owl-carousel2/owl.carousel.min.js"></script>
        <script src="/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="/plugins/isotope-master/dist/isotope.pkgd.min.js"></script>

        <!-- Page JS -->      
        <script src="/js/jquery.sticky.js"></script>
        <script src="/js/custom.js"></script>
    </body>
</html>