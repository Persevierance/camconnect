<div id="owl-carousel-main" class="owl-carousel nav-1">
	<div class="gst-slide">
		<img src="/img/slides/1.png" alt=""/>
		<div class="gst-caption container theme-container">
			<div>
				<div class="caption-right">
					<h3 class="fsz-40 wht-clr funky-font-2"> TheCamConnect's Latest Arrival </h3>
<!--					<h2>TheCamConnect<span class="thm-clr"></span></h2>-->
					<p class="">Photography is a healthy, fun and exciting way to look at the world. No matter what type of photography you're into, we've got a Camera for you.</p>
					<a class="fancy-btn-alt" href="#">Shop Now</a>
				</div>
			</div>
		</div>
	</div>
	<div class="gst-slide">
		<img src="/img/slides/3.png"  alt=""/>
		<div class="gst-caption container theme-container">
			<div>
				<div class="caption-center">

					<p class="slider-title"> <span class="fsz-220 funky-font"></span> <span class="funky-font-2 fsz-28">Camera Accessories, and much more Anytime</span> </p>
					<a class="fancy-btn" href="#">Find Yours</a>
				</div>
			</div>
		</div>
	</div>
	<div class="gst-slide">
		<img src="/img/slides/2.png" alt=""/>
		<div class="gst-caption container theme-container">
			<div>
				<div class="caption-center">
					<p class="slider-title"> <span class="fsz-220 funky-font"><!--Photography--> </span> <span class="funky-font-2 fsz-35"> <br />Our Cameras are better <br> than you can ever imagine </span> </p>
					<a class="fancy-btn" href="#">Find Yours</a>
				</div>
			</div>
		</div>
	</div>
</div>
