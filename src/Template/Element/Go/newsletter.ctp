<!-- Subscribe News -->
<section class="gst-row gst-color-white row-newsletter ovh">
	<div class="gst-wrapper">
		<div class="gst-column col-lg-12 no-padding text-center">
			<div class="fancy-heading text-center">
				<h3 class="wht-clr">Subscribe Newsletter</h3>
				<h5 class="funky-font-2 wht-clr">Sign up for <span class="thm-clr">Special Promotion</span></h5>
			</div>

			<div class="gst-empty-space clearfix"></div>
			
			<form>
				<div class="col-md-2"><h4><strong class="fsz-20"> <span class="thm-clr">Subscribe</span> to us </strong>
					</h4></div>
				<div class="gst-empty-space visible-sm clearfix"></div>
				<div class="col-md-4 col-sm-4">
					<input type="text" class="dblock" placeholder="Enter your name"/>
				</div>
				
				<div class="col-md-4 col-sm-4">
					<input type="text" class="dblock" placeholder="Enter your email address"/>
				</div>
				
				<div class="col-md-2 col-sm-4">
					<input type="submit" class="dblock fancy-button" value="Submit"/>
				</div>
			</form>
		</div>
	</div>
</section>
<!-- / Subscribe News -->