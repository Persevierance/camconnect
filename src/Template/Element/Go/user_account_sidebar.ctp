<div class="main-sidebar" >
	<div id="search-2" class="widget sidebar-widget widget_search clearfix">
		<form method="get" id="searchform" class="form-search" action="http://localhost/goshopwp">
			<input class="form-control search-query" type="text" placeholder="Type Keyword" name="s" id="s" />
			<button class="btn btn-default search-button" type="submit" name="submit"><i class="fa fa-search"></i></button>
		</form>
	</div>
	<div class="widget sidebar-widget widget_categories clearfix">
		<h6 class="widget-title">My Account</h6>
		<ul>
			<li  class="accout-item"><a href="/Shop/account_information"> Account Information </a></li>
			<li  class="accout-item active"><a href="/Shop/my_account">My Account</a></li>
			<li  class="accout-item"><a href="/Shop/change_password">Change Password</a></li>
			<li  class="accout-item"><a href="/Shop/address_book">Address Books</a></li>
			<li  class="accout-item"><a href="/Shop/order_hostory">Order History</a></li>
			<li  class="accout-item"><a href="/Shop/reviews">Reviews and Ratings</a></li>
			<li  class="accout-item"><a href="/Shop/returns">Returns Requests</a></li>
		</ul>
	</div>
</div>
