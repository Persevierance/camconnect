<div class="header-wrap" id="typo-sticky-header">
	<div class="container theme-container reltv-div">
		<div class="pull-right header-search visible-xs">
			<a id="open-popup-menu" class="nav-trigger header-link-search" href="javascript:void(0)" title="Menu">
				<i class="fa fa-bars"></i>
			</a>
		</div>

		<div class="row">
			<div class="col-md-3 col-sm-3">
				<div class="top-header pull-left">
					<div class="logo-area">
						<a href="index.html" class="thm-logo fsz-35">
							<img src="/img/logo/logotiny.png" class="img-responsive" width="100" alt="CamConnect Logo">
						</a>
					</div>
				</div>
			</div>
			<!-- Navigation -->

			<div class="col-md-9 col-sm-9 static-div">
				<div class="navigation pull-left">
					<nav>
						<div class="" id="primary-navigation">
							<ul class="nav navbar-nav primary-navbar">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true">Home</a>
								</li>
								<li class="active"><a href="/Shop"> SHOP </a></li>
								<li class="dropdown mega-dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
									   aria-haspopup="true">STILLS</a>
									<div class="dropdown-menu mega-dropdown-menu mega-styl2"
									     style="background: white no-repeat url(/img/extra/megamenu_stills.png) right 25px center; ">
										<div class="col-sm-6 menu-block">
											<div class="sub-list">
												<ul>
													<li><a href="#"> Lenses </a></li>
													<li><a href="#"> Compact </a></li>
													<li><a href="#"> Lighting </a></li>
													<li><a href="#"> Mirrorless </a></li>
													<li><a href="#"> Digital SLR </a></li>
													<li><a href="#"> Accessories </a></li>
													<li><a href="#"> Medium Format </a></li>
												</ul>
											</div>
										</div>
									</div>
								</li>
								<li class="dropdown mega-dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
									   aria-haspopup="true"> Motion </a>
									<div class="dropdown-menu mega-dropdown-menu mega-styl2"
									     style="background: white no-repeat url(/img/extra/megamenu_motion.png) right 25px center; ">
										<div class="col-sm-6 menu-block">
											<div class="sub-list">
												<ul>
													<li><a href="#"> Sound </a></li>
													<li><a href="#"> Lenses </a></li>
													<li><a href="#"> Cameras </a></li>
													<li><a href="#"> Lighting </a></li>
													<li><a href="#"> Accessories </a></li>
													<li><a href="#"> Tripods & Grip </a></li>
													<li><a href="#"> Monitors & Viewfinders </a></li>
												</ul>
											</div>
										</div>
									</div>
								</li>
								<li><a href="/Shop/Blog">Blog</a></li>
								<li><a href="/Shop/contact">Contact</a></li>
							</ul>
						</div>
					</nav>
				</div>
				<div class="pull-right srch-box">
					<a id="open-popup-search" class="header-link-search" href="javascript:void(0)" title="Search">
						<i class="fa fa-sesarch"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>