
<!-- Popup: Login 1 -->
<div class="modal fade login-popup" id="login-popup" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<button type="button" class="close close-btn popup-cls" data-dismiss="modal" aria-label="Close"><i
				class="fa-times fa"></i></button>

		<div class="modal-content login-1 wht-clr">
			<div class="login-wrap text-center">
				<div class="fancy-heading text-center">
					<h3><span class="thm-clr">Account </span> Login</h3>
				</div>
				<h2 class="funky-font-2"> WELCOME TO OUR WONDERFUL WORLD OF PHOTOGRAPHY</h2>
				<p class="fsz-15 bold-font-4"> You can not hire better and cheaper camera equipment <span class="thm-clr"> anywhere else. </span></p>
				<div class="login-form">
					<div class="text-center"><?= $this->Flash->render('auth');?></div>
					<a class="fb-btn btn spcbtm-15" href="#"> <i class="fa fa-facebook btn-icon"></i>Login with Facebook</a>
					<p class="bold-font-2 fsz-12 signup"> OR <a class="wht-clr" href="/users/register">SIGN UP</a> </p>
					<?=$this->Form->create('login',['class'=>'login','method'=>'post']);?>
					<div class="form-group"><input type="text" name="username" placeholder="Email" class="form-control"></div>
					<div class="form-group"><input type="text" name="password" placeholder="Password" class="form-control"></div>
					<div class="form-group">
						<button class="alt fancy-button" type="submit"><span class="fa fa-lightbulb-o"></span> Login
						</button>
					</div>
					<?=$this->Form->end();?>
					<p>* Denotes mandatory field.</p>
					<p>** At least one telephone number is required.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /Popup: Login 1 -->
