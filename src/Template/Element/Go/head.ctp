<head>
	<meta charset="utf-8">
	<!--[if IE]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>The Cam Connect</title>
	<!-- Favicon -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/ico/apple-touch-icon-144-precomposed.png">
	<link rel="shortcut icon" href="/ico/favicon.ico">
	<!-- CSS Global -->
	<link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="/plugins/datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
	<link href="/plugins/bootstrap-select-1.9.3/dist/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
	<link href="/plugins/owl-carousel2/assets/owl.carousel.css" rel="stylesheet" type="text/css">
	<link href="/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.min.css" rel="stylesheet"
	      type="text/css">
	<link href="/plugins/royalslider/skins/universal/rs-universal.css" rel="stylesheet">
	<link href="/plugins/royalslider/royalslider.css" rel="stylesheet">
	<link href="/plugins/subscribe-better-master/subscribe-better.css" rel="stylesheet" type="text/css">

	<!-- Icons Font CSS -->
	<link href="/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!-- Theme CSS -->
	<link href="/css/style.css" rel="stylesheet" type="text/css">
	<link href="/css/header.css" rel="stylesheet" type="text/css">
	<link href="/css/helper.css" rel="stylesheet" type="text/css">

	<!--[if lt IE 9]>
	<script src="/plugins/iesupport/html5shiv.js"></script>
	<script src="/plugins/iesupport/respond.js"></script>
	<![endif]-->
</head>
