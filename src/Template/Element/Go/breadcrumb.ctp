<?php
	if(!isset($url)){
		$url = explode('/', $this->request->here());
		$url[1] = 'myAccount';
	}
?>

<div class="site-pagetitle jumbotron">
	<div class="container theme-container text-center">
		<h3></h3>
		<!-- Breadcrumbs -->
		<div class="breadcrumbs">
			<div class="breadcrumbs text-center">
				<i class="fa fa-home"></i><span><a href="index.html">Home</a></span><i class="fa fa-arrow-circle-right"></i>
				<span><a href="/<?=$url[1];?>/"><?=$url[1];?></a></span><i class="fa fa-arrow-circle-right"></i>

				<?php if(!empty($url[3])){ ?>
					<span><a href="/<?=$url[1]?>/<?=$url[2]?>"><?=$url['2'];?></a></span><i class="fa fa-arrow-circle-right"></i>
					<span class="current"><?=$url['3'];?></span>
				<?php } else {?>
					<span class="current"><?=$url['2'];?></span>
				<?php } ?>
			</div>
		</div>
	</div>
</div>