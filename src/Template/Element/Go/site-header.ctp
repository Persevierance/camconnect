<div class="site-subheader site-header">
	<div class="container theme-container">
		<!-- Language & Currency Switcher -->
<!--		<ul class="pull-left list-unstyled list-inline">
			<li class="nav-dropdown language-switcher">
				<div>EN</div>
				<ul class="nav-dropdown-inner list-unstyled list-lang">
					<li><span class="current">EN</span></li>
					<li><a title="Russian" href="#">RU</a></li>
					<li><a title="France" href="#">FR</a></li>
					<li><a title="Brazil" href="#">IT</a></li>
				</ul>
			</li>
			<li class="nav-dropdown language-switcher">
				<div><span class="fa fa-dollar"></span>USD</div>
				<ul class="nav-dropdown-inner list-unstyled list-currency">
					<li><span class="current"><span class="fa fa-dollar"></span>USD</span></li>
					<li><a title="Euro" href="#"><span class="fa fa-eur"></span>Euro</a></li>
					<li><a title="GBP" href="#"><span class="fa fa-gbp"></span>GBP</a></li>
				</ul>
			</li>
		</ul> -->

		<!-- Mini Cart -->
		<ul class="pull-right list-unstyled list-inline">
			<li class="nav-dropdown">
				<a href="#">My Account</a>
				<ul class="nav-dropdown-inner list-unstyled accnt-list">
					<?php if(!empty($user)){?>
						<li><a href="/User/">My Account</a></li>
						<li><a href="/User/accountInformation"> Account Information </a></li>
						<li><a href="/User/changePassword">Change Password</a></li>
						<li><a href="/User/addressBook">Address Books</a></li>
						<li><a href="/User/orderHistory">Order History</a></li>
						<li><a href="/Users/logout">Logout</a></li>
					<?php } else { ?>
						<li><a href="/Users/Login">Login</a></li>
						<li><a href="/Users/Register">Register</a></li>
					<?php } ?>
				</ul>
			</li>
			<li id="cartContent" class="cartContent">
				<a id="miniCartDropdown" href="cart.html">My Cart<span class="cart-item-num">0</span></a>
				<!--<div id="miniCartView" class="cartView">
					<ul id="minicartHeader" class="product_list_widget list-unstyled">
						<li>
							<div class="media clearfix">
								<div class="media-lefta">
									<a href="single-product.html">
										<img src="/img/products/theme/cart-popup-1.jpg" alt="hoodie_5_front"/>
									</a>
								</div>
								<div class="media-body">
									<a href="single-product.html">Flusas Feminin</a>
									<span class="price"><span class="amount"><span class="fa fa-dollar"></span>20.00</span></span>
									<span class="quantity">Qty:  1Pcs</span>
								</div>
							</div>

							<div class="product-remove">
								<a href="#" class="btn-remove" title="Remove this item"><i class="fa fa-close"></i></a>
							</div>
						</li>
					</ul>

					<div class="cartActions">
						<span class="pull-left">Subtotal</span>
						<span class="pull-right"><span class="amount"><span class="fa fa-dollar"></span>75.00</span></span>
						<div class="clearfix"></div>

						<div class="minicart-buttons">
							<div class="col-lg-6">
								<a href="cart.html">Your Cart</a>
							</div>
							<div class="col-lg-6">
								<a href="/Shop/checkout" class="minicart-checkout">Checkout</a>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>-->
			</li>
			<li class="menu-item"><a href="/Shop/checkout">Checkout</a></li>
			<li class="menu-item">
				<?php if(empty($user)){?>
					<a href="#login-popup" data-toggle="modal">Login</a>
				<?php } else {?>
					<a href="/Users/logout">Logout </a>
				<?php } ?>
			</li>
		</ul>
	</div>
</div>
